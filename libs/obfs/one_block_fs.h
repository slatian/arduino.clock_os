// Copyright 2024 Slatian <baschdel@disroot.org>
// SPDX-FileContributor: Slatian
//
// SPDX-License-Identifier: LGPL-3.0-only

#include <stdint.h>
#include <stdbool.h>

#ifndef one_block_fs_h_INCLUDED
#define one_block_fs_h_INCLUDED

/*
 * Data types
 */

// Data decriptor types.
// 0xf0 to 0xff are reserved for the fs itself
// 0x00 to 0xef are free for use
#define OBFS_DDT_UNUSED_DESCRIPTOR 0xf0
#define OBFS_DDT_EMPTY 0xfe

struct obfs_data_descriptor_t {
	uint8_t type;
	uint8_t next_descriptor;  //0 means end of chain 
	uint16_t size;
	uint16_t offset;
};

typedef struct obfs_data_descriptor_t obfs_data_descriptor_t;
#define DATA_DESCRIPTOR_SIZE sizeof(obfs_data_descriptor_t)

// !afs -> not a fs
#define OBFS_MAGIC_1 0x1b
#define OBFS_MAGIC_2 0xf5

struct obfs_header {
	uint8_t magic_1; // 0x1a
	uint8_t magic_2; // 0xf5
	uint8_t version; // 0x01
	uint8_t header_size; // 0x06
	uint8_t num_descriptors;
	uint8_t reserved;
};

typedef struct obfs_header obfs_header_t;

/*
 * Function ressult type
 */
#define OBFS_OK 0
#define OBFS_ERROR 0xff

// PATCH: typedef and arduino aren't friends
//typedef uint8_t obfs_res;
#define obfs_res uint8_t

/*
 * Storage driver functions
 * (Not implemented by not a fs, Implement these yourself)
 */

#define OBFS_STORAGE_ERROR_GENERIC 100
#define OBFS_STORAGE_ERROR_UNKNOWN_DRIVE 101
#define OBFS_STORAGE_ERROR_OUT_OF_BOUNDS 102
#define OBFS_STORAGE_ERROR_WRITE_DENIED 103

//Feel free to use the range 150 to 199 for your own storage error codes

// Returns the number of bytes that fit on a given drive
// if 0 obfs assumes that the drive doesn't exist
uint16_t obfs_storage_get_size(uint8_t drive);

// Read size bytes from the drive starting at at_offset into buffer.
// When called by obfs a check against a freshly fetched
// obfs_get_storage_size() has aleady happend to ensure the read
// is in bounds on a valid drive.
obfs_res obfs_storage_read_data(
	uint8_t drive,
	uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
);

// Write size bytes to the drive starting at at_offset from buffer.
// If buffer == NULL write 0s instead!!!
// When called by obfs a check against a freshly fetched
// obfs_get_storage_size() has aleady happend to ensure the read
// is in bounds on a valid drive.
obfs_res obfs_storage_write_data(
	uint8_t drive,
	const uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
);


/*
 * Functions provided by one block fs
 */

#define OBFS_FORMAT_FS_ERROR_NOT_ENOUGH_DESCRIPTORS 1
#define OBFS_FORMAT_FS_ERROR_WAY_TOO_MANY_DESCRIPTORS 2
#define OBFS_FORMAT_FS_ERROR_HEADER_WRITE_FAIL 3
#define OBFS_FORMAT_FS_ERROR_EMPTY_SECTOR_WRITE_FAIL 4
#define OBFS_FORMAT_FS_ERROR_DESCRIPTOR_INIT_WRITE_FAIL 5

obfs_res obfs_format_fs(uint8_t drive, uint8_t num_of_descriptors);

#define OBFS_ALLOCATE_ERROR_NO_FREE_SPACE_FOUND 10
#define OBFS_ALLOCATE_ERROR_NO_UNUSED_DESCRIPTOR_FOUND 11
#define OBFS_ALLOCATE_ERROR_UPDATNG_LEFTOVERS_FAILED 12
#define OBFS_ALLOCATE_ERROR_UPDATING_NEW_FAILED 13

obfs_res obfs_allocate_descriptor(
	uint8_t drive,
	uint8_t* new_descriptor_id,
	obfs_data_descriptor_t* new_descriptor,
	uint16_t size,
	uint8_t type
);

#define OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_READ_DESCRIPTOR 20
#define OBFS_FREE_DESCRIPTOR_ERROR_DESCRIPTOR_EMPTY_OR_UNUSED 21
#define OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_READ_POTENTIAL_PREVIOUS_DESCTIPTOR 22
#define OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_READ_NEXT_DESCTIPTOR 23
#define OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_UPDATE_PREVIOUS_DESCRIPTOR 24
#define OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_UPDATE_DESCRIPTOR 25
#define OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_UPDATE_NEXT_DESCRIPTOR 26

obfs_res obfs_free_descriptor(
	uint8_t drive,
	uint8_t descriptor_id,
	bool clear
);

#define OBFS_DESCRIPTOR_IO_ERROR_OUT_OF_BOUNDS 30

obfs_res obfs_descriptor_read_data(
	uint8_t drive,
	const obfs_data_descriptor_t* descriptor,
	uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
);

obfs_res obfs_descriptor_write_data(
	uint8_t drive,
	const obfs_data_descriptor_t* descriptor,
	const uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
);

#define OBFS_FIND_DESCRIPTOR_OF_TYPE_NOT_FOUND 50
#define OBFS_FIND_DESCRIPTOR_OF_TYPE_ERROR_UNABLE_TO_READ_DESCRIPTOR 51

// result gets written to desc, calling again (with first_call = false)
// will return the next matching descriptor.
// neither descriptor_id, nor descriptor may be null.
// descriptor will be overwritten.
obfs_res obfs_find_descriptor_of_type(
	uint8_t drive,
	uint8_t* descriptor_id,
	obfs_data_descriptor_t* descriptor,
	uint8_t type_mask,
	uint8_t type,
	bool first_call
);

obfs_res obfs_read_data_descriptor(
	uint8_t drive,
	uint8_t descriptor_id,
	obfs_data_descriptor_t* desc
);

// Returns the number of file descriptors, 0 on the number being unavailable
uint8_t obfs_get_number_of_file_descriptors(uint8_t drive);

#endif // one_block_fs_h_INCLUDED
