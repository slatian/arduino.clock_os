/*
	Utils.ino - Some useful functions for Clock OS
*/

/*
	Provides:
	* get_hex_value(char c)
		returns the value of c as a hexadecimal digit, returns 0 if c os not a hex digit

*/

const char hex_string[] = "0123456789ABCDEF";

uint8_t get_hex_value(char c) {
	if ('0' <= c <= '9') {
		return c-'0';
	} else if ('A' <= c <= 'F') {
		return c-'A'+10;
	} else if ('a' <= c <= 'f') {
		return c-'a'+10;
	} else {
		return 0;
	}
}

// Will write two hex chars to str representing n
void write_hex_value(uint8_t n, char* str) {
	str[0] = hex_string[(n >> 4 & 0x0f)];
	str[1] = hex_string[(n & 0x0f)];
}
