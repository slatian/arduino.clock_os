//************libraries**************//

// External Libraries
#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <RTClib.h>
#include <Wire.h>

// Local Libraries
#include <memory_free.h>

// Sketch includes
#include "alarm.h"
#include "color.h"
#include "notification.h"
#include "storage.h"
#include "timekeeping.h"
#include "ui.h"

// Hardware configuration
#include "Clock_v1_hardware.h"

// We have a lcd object
LiquidCrystal lcd(LCD_RS_PIN, LCD_EN_PIN, LCD_D4_PIN, LCD_D5_PIN, LCD_D6_PIN, LCD_D7_PIN);

//********Settings Variables*********//

//Using a String here is actually cheaper because we're using Strings anyway
char setting_time_format[] = "DDD MM-DD  hh:mm";

int setting_minutes_snooze_time = 5;
int setting_give_up_after_aggregate_snooze_time_of = 60;
int setting_ms_per_caracter_scroll = 750;


theme_settings_file theme_settings;

//************Variables**************//
// store UI mode
enum ui_mode_t {
	CLOCK,
	BIG_CLOCK,
	TIME_EDITOR,
	DATE_EDITOR,
	SETTINGS_MENU,
	NUMBER_EDITOR,
	COLOR_EDITOR,
	ALARM_SELECTOR,
	ALARM_EDITOR,
	UI_INACTIVE, //Dummy mode used for disabling repeat on first button press after wakeup
};

enum time_editor_save_action_t {
	ADJUST,
	SET_ALARM,
};

// don't update the clock every tick 
unsigned long last_time_update = 0; //time should be updated once per second
unsigned long last_cron_update = 0; //cronjobs run every 10 seconds

// debounce and repeat button presses
int last_button = -1;
uint8_t button_repeat_counter = 0;
unsigned long last_button_press_time = 0;
unsigned long last_button_repeat_time = 0;
unsigned long last_button_release_time = 0;
ui_mode_t button_down_ui_mode = CLOCK;

ui_mode_t ui_mode = CLOCK;
time_editor_save_action_t time_editor_save_action = ADJUST;
byte currently_selected_alarm = 0;
bool was_recently_active = true;

Notification notification;

bool play_alert_melody = false;
unsigned long player_note_started_at = 0;
unsigned int player_note_length = 0;

// How many alarms are available
// Note: going above 10 (Alarms 0 to 9) will work
// but the display logic isn't prepared for two digit
// numbers, the additional digits will be ignored
#define N_ALARMS 8
alarm_t alarms[N_ALARMS] = {};

void setup() {
	pinMode(BTN_OK,    INPUT_PULLUP);
	pinMode(BTN_UP,    INPUT_PULLUP);
	pinMode(BTN_DOWN,  INPUT_PULLUP);
	pinMode(BTN_LEFT,  INPUT_PULLUP);
	pinMode(BTN_RIGHT, INPUT_PULLUP);
	
	pinMode(RGB_R_PIN, OUTPUT);
	pinMode(RGB_G_PIN, OUTPUT);
	pinMode(RGB_B_PIN, OUTPUT);
	pinMode(BEEPER_PIN, OUTPUT); //Audio ring indicator

	startup_serial_interface();
	
	lcd.begin(16, 2);
	lcd.clear();
	ui_initalize();

	Wire.begin();

	// Try to restore settings from eeprom
	// If that fails, write our initial settings and send a notification
	if (!restore_from_eeprom()) {
		save_to_eeprom(true);
		set_notification(NOTIFY_NORMAL, F("Initalized EEPROM!"), NOTIFICATION_ORIGIN_NONE, 60, 0);
		// Set some default values
		theme_settings.version = 0;
		theme_settings.flags = 0x01;
		theme_settings.color_backlight = {0x88, 0xbb, 0};
		theme_settings.color_notification = {0, 0xff, 0xff};
	}

	// Set timer/counter 1 from 8-bit phase correct to 8-bit fast
	bitWrite(TCCR1B, WGM12, 1);
	// Turn on backlight after reading settings
	set_rgb_color(theme_settings.color_backlight);

	/**** Initalize Clock ****/

	ui_set_title(F("RTC init ..."));
	if (timekeepinng_initalize_clocks(&Wire)) {
		ui_set_title(F("Real RTC running!"));
	} else {
		ui_set_title(F("Real RTC failed."));
	}
	delay(500);
	
	// Set current time to have it ready for initalization
	// This is done after restoring because the summertime setting comes from EEPROM
	timekeeping_update_now();

	// Reserve some memory for strings to keep memory fragmentation to a minimum
	notification.message.reserve(48);
	ui_current_message.reserve(48);

	// Send a little startup notification because why not …
	set_notification(NOTIFY_LOW, F("Tick tock, goes the clock, Even for the doctor."), NOTIFICATION_ORIGIN_NONE, 60, 0);

	delay(50);
	start_clock_face();
}

void loop() {
	//// Some visual housekeeping
	// Keep current message scrolling
	// if not set the function detects that by itself
	ui_update_message();

	//Update time from rtc every second
	if (millis() - last_time_update >= 1000) {
		last_time_update = millis();
		timekeeping_update_now();
		check_notification_expired();

		// Update clock
		if (ui_mode == CLOCK || ui_mode == BIG_CLOCK) {
			ui_update();
		// if nothing happens in 2 minutes show the clock
		} else if (millis() - last_button_release_time > 120000) {
			start_clock_face();
		}

		// Make ui inactive after 20 seconds of no interaction
		if (millis() - max(last_button_release_time, last_button_press_time) > 20000) {
			if (notification.mode > NOTIFY_NORMAL) {
				was_recently_active = false;
				ui_update();
			}
		}

	}

	// Run periodic, less common jobs
	// This should be okayish rollover proof
	if (millis() - last_cron_update >= 10000) {
		last_cron_update = millis();

		//Trigger alarms
		for (int i = 0; i < N_ALARMS; i++) {
			alarm_t *alarm = &alarms[i];
			if (alarm_matches_timestamp(alarm, now)) {
				if (!alarm->was_triggerd) {
					alarm->was_triggerd = true;
					char *message_buf = "Alarm #0 at 00:00";
					message_buf[7] = '0'+(i%10);
					alarm_time_to_string(alarm->settings.minutes_after_midnight, message_buf+12);
					set_notification(NOTIFY_RING, message_buf, i, 120, 0);
				}
			} else {
				alarm->was_triggerd = false;
			}
		}

		// Save to eeprom if there is data that was altered
		save_to_eeprom(false);

		timekeeping_syncronize_rtc();
	}	

	//get Button press and repeat if button is held down
	int button = get_currently_pressed_button();
	if (button != last_button) {
		last_button = button;
		if (button >= 0) {
			last_button_press_time = millis();
			if (was_recently_active) {
				button_repeat_counter = 0;
				button_down_ui_mode = ui_mode;
				on_button_down(button, 0);
			} else {
				// Discard button presses that woke the device up
				button_down_ui_mode = UI_INACTIVE;
			}
		} else {
			last_button_release_time = millis();
		}
		was_recently_active = true;
		ui_update();
	}
	// Repeat held down buttons but not across ui modes
	if (
		last_button >= 0 && 
		millis()-last_button_press_time > 500 &&
		millis()-last_button_repeat_time >= 50 && 
		button_down_ui_mode == ui_mode
	) {
		if (button_repeat_counter < 0xff) {
			button_repeat_counter++;
		}
		on_button_down(last_button, button_repeat_counter);
		last_button_repeat_time = millis();
	}
	
	//currently we only have one "random" melody
	if (play_alert_melody) {
		if (millis() - player_note_started_at > player_note_length) {
			int duration = random(1,3)*250;
			player_note_started_at = millis();
			player_note_length = duration*1.3;
			tone(BEEPER_PIN, random(200, 1000), duration);
		}
	} else {
		noTone(BEEPER_PIN);
	}

	task_serial_interface();

	
	//give our processor some rest
	delay(50);
}

void update_leds() {
	// approximately 2s timer loop (2048ms)
	unsigned long blink_timer = millis()&0x0FFF;
	// Blink signal
	bool blink = blink_timer & 0x0800; //toggle every second
	bool fast_blink = blink_timer & 0x0400; //toggle every half second
	// blink to show that we are alive
	digitalWrite(LED_PIN, is_unsaved_data_flag() ? fast_blink : blink);

	// update led colors
	if (notification.mode == NOTIFY_NORMAL) {
		set_rgb_color(theme_settings.color_notification);
	} else if (notification.mode <= NOTIFY_URGENT) {
		if (blink) {
			set_rgb_color(theme_settings.color_notification);
		} else {
			set_rgb_color(theme_settings.color_backlight);
		}
	} else if (was_recently_active) {
		if (ui_mode != COLOR_EDITOR) {
			set_rgb_color(theme_settings.color_backlight);
		}
	} else {
		set_rgb_color({0,0,0});
	}
}

void set_rgb_color(color_t color) {
	analogWrite(RGB_R_PIN, 255-color.r);
	analogWrite(RGB_G_PIN, 255-color.g);
	analogWrite(RGB_B_PIN, 255-color.b);
}

void send_free_memory_notification() {
	set_notification(NOTIFY_LOW, String("Free Memory: ")+String(freeMemory()), NOTIFICATION_ORIGIN_NONE, 60, 0);
}


bool set_notification(notification_mode_t mode, String message, short origin, int32_t expire_in, long cookie) {
	check_notification_expired();
	if (notification.mode > mode) {
		// Treat a notification as activity
		was_recently_active = mode <= NOTIFY_NORMAL;
		if (notification.origin != NOTIFICATION_ORIGIN_NONE) {
			dismiss_notification(RESPONSE_SNOOZE); // Come back later (or don't)
		}
		notification.message = message;
		notification.mode = mode;
		notification.origin = origin;
		notification.expires_at = now+TimeSpan(expire_in);
		notification.time_sent = now;
		notification.serial++;
		notification.cookie = cookie;

		ui_update();

		play_alert_melody = notification.mode == NOTIFY_RING;
		//send the notification over the wire
		send_notification(0);
	} else {
		// High priority notifications shouldn't get dismissed because of a lower priority one
		return false;
	}
}

inline void check_notification_expired() {
	if (notification.expires_at < now && notification.mode != NO_NOTIFICATION) {
		dismiss_notification(RESPONSE_EXPIRE);
	}	
}

// This function may receive all kinds of responses from the serial interface, default to quietly discarding the notification
void dismiss_notification(notification_response_t response) {
	if (notification.mode == NO_NOTIFICATION) { return; }

	if (notification.origin >= 0 && notification.origin < N_ALARMS) {
		alarm_t *alarm = &alarms[notification.origin];
		if (response == RESPONSE_DISMISS || alarm_is_over_snooze_time(alarm)) {
			// give up after 60 minutes of aggregated snooze time
			alarm->minutes_snooze_time=0;
			if (alarm->settings.flags == 0x80) {
				alarm->settings.flags = 0; //disable an alarm set to once
				flag_unsaved_data();
			}
		} else if (response == RESPONSE_SNOOZE) {
			if (alarm->settings.snooze_delay_minutes) {
				alarm->minutes_snooze_time += alarm->settings.snooze_delay_minutes;
			} else {
				alarm->minutes_snooze_time += setting_minutes_snooze_time;
			}
		}
	}
	
	//send the notification over the wire
	send_notification(response);
	
	notification.mode = NO_NOTIFICATION;
	notification.message = "";
	notification.origin = NOTIFICATION_ORIGIN_NONE;
	notification.cookie = 0;
	play_alert_melody = false;

	ui_update();
}

//*********Helper Functions********//


// repeat_count is the number of repeats that have
// been emitted since the button was pressed down.
// When 0xff is reached it stops incrementing.
void on_button_down(int button, uint8_t repeat_count) {
	switch(ui_mode) {
		case DATE_EDITOR:
		case TIME_EDITOR:
			edit_time_handle_button(button, repeat_count);
			break;
		case SETTINGS_MENU:
			settings_menu_handle_button(button);
			break;
		case NUMBER_EDITOR:
			number_editor_handle_button(button, repeat_count);
			break;
		case COLOR_EDITOR:
			color_editor_handle_button(button, repeat_count);
			break;
		case ALARM_SELECTOR:
			alarm_selector_handle_button(button);
			break;
		case ALARM_EDITOR:
			alarm_editor_handle_button(button);
			break;
		case CLOCK:
			clock_face_handle_button(button, repeat_count);
			break;
		default:
			break;
	}
}

// For ui modes that react to events like
// the active mode changing or a notification.
void ui_update() {
	update_leds();
	switch (ui_mode) {
		case CLOCK:
			update_clock_face();
			break;
		case BIG_CLOCK:
			update_big_clock();
			break;
	}
}

void ui_go_back() {
	ui_clear();
	// Currently uses some hacky heutistics
	// Also flags data as unsaved in some places
	// the EEPROM library does the checking
	// for already written stuff anyway.
	switch (ui_mode) {
		case ALARM_EDITOR:
			flag_unsaved_data();
			switch_ui_mode_to(ALARM_SELECTOR);
			return;
		case TIME_EDITOR:
		case DATE_EDITOR:
			if (time_editor_save_action == SET_ALARM) {
				switch_ui_mode_to(ALARM_EDITOR);
			} else {
				switch_ui_mode_to(SETTINGS_MENU);
			}
			return;
	}
	start_clock_face();
}

void switch_ui_mode_to(ui_mode_t mode) {
	switch (mode) {
		case ALARM_SELECTOR:
			ui_mode = ALARM_SELECTOR;
			update_alarm_selector_visuals();
			break;
		case ALARM_EDITOR:
			ui_mode = ALARM_EDITOR;
			update_alarm_editor_visuals();
			break;
		case CLOCK:
			start_clock_face();
			break;
		case SETTINGS_MENU:
			ui_mode = SETTINGS_MENU;
			update_settings_visuals(true);
			break;
	}
}

int get_currently_pressed_button() {
	if (digitalRead(BTN_OK) == LOW) {
		return BTN_OK;
	} else if (digitalRead(BTN_DOWN) == LOW){
		return BTN_DOWN;
	} else if (digitalRead(BTN_UP) == LOW){
		return BTN_UP;
	} else if (digitalRead(BTN_LEFT) == LOW){
		return BTN_LEFT;
	} else if (digitalRead(BTN_RIGHT) == LOW){
		return BTN_RIGHT;
	}
	return -1;
}

