#ifndef notification_h_INCLUDED
#define notification_h_INCLUDED

// Include the RTClib made by Adafruit for the DateTime struct
#include <RTClib.h>

#define NOTIFICATION_ORIGIN_NONE -1
#define NOTIFICATION_ORIGIN_EXTERNAL -2

enum notification_mode_t {
	NOTIFY_RING = 0,
	NOTIFY_URGENT = 1,
	NOTIFY_NORMAL = 2,
	NOTIFY_LOW = 3,
	NO_NOTIFICATION = 4,
};

enum notification_response_t : char {
	RESPONSE_SNOOZE = 'd',
	RESPONSE_EXPIRE = 'e',
	RESPONSE_DISMISS = 'a',
};

struct Notification {
	String message = "";
	notification_mode_t mode = NO_NOTIFICATION;
	short origin = -1; // >= 0 means alarm number
	long cookie = 0;
	long serial = 0;
	DateTime expires_at = DateTime();
	DateTime time_sent = DateTime();
};

#endif // notification_h_INCLUDED

