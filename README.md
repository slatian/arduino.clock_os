# Clock OS v1

This is the sourcecode for my Arduino UNO based alarm clock that is waking me up five mornings a week.

## Hardware

* An ATMEL ATMega238P (CPU used in the Arduino UNO) (+ 2 22pf caps and a 16MHz crystal)
* A 16x2 caracter LCD (with RGB Backlight) (remember that you need a potentiometer for adjusting contrast)
* 5 Buttons (I use the `Alps SKQU 4 Direction Joystick with Switch`)
* A Real time Clock (In my case a DS1307, Adafruit made a nice library and sells breakouts)
* A generic Piezio Beeper that can be used with 5 Volts

**Wiring changes:**
* On 2024-01-28 pins 2 and 3 have swapped functionality to have 3 PWM pins for the RGB backlights in the default configuration.
* On 2024-04-08 pins 3 and 5 have swapped functionality to avoid the [tone() function](https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/) interfering with the backlight.

## Features

* A Clockface showing the current day, date and time + a status message
* UI for setting date and time
* 8 Alarms, saved to EEPROM
* Alarms can be sceduled for certain weekdays only or just ring once
* Configurable Snooze timer (settings menu)
* Hardware RTC is optional at the cost of not keeping time across resets
* Currently experimental and unstable interface over Serial
* Plays a pseudorandom Melody to wake you up so you don't get mad at that one Melody
* RGB Support for LCD Backlight (currently the colors are still hardcoded.)

## Screenshots

I could post bandwidth wasting pictures here but codeblocks are the better fit for this I think …

### Clockface with an Alarm

Note that the status message scolls across the screen if it is too long.

```
Sun 09-18  21:47
Alarm #0 at 21:4
```

### Alarm Selector

Shows the currently selected alarm Number 4 set to trigger at 05:40 on Mondays and Wednesdays.

```
Select Alarm:
4>05:40 M W
```

## Usage

The UI knows about 5 Buttons: OK, UP, DOWN, LEFT, RIGHT

Note: If you leave the UI for too long it cancels the current menu and shows the clockface
### General Rules

* UP and DOWN are usually used for selecting a menu item or in/decrementing a number.
* RIGHT is the equivalent to OK in most cases.
* LEFT is usually the back button.
* Holding a button will repeat the action unless the action switched to a differenct screen.
* OK/LEFT activates the currently selected menu entry and brings.

### If you have a cursor on screen

* LEFT and RIGHT move the cursor.
* Holding LEFT means cancel.
* Pressing OK means accept the current value as the new setting.

### On the Clockface

* RIGHT brings you to the settings menu.
* UP fires a low priority notification that tells you how many bytes of memory are free for dynamic allocation.
* DOWN dismisses a notification and snoozes an alarm if present.
* DOWN takes you to the alarm list for setting alarms if no notification is present.
* OK dismisses a notification or alarm (the alarm won't be scedules for snoozing).
* If no notification or alarm is active (no status message) DOWN brings you to the Alarm selector for setting alarms.

### Settings Menu

Adjust Time
: Set the current Time for the Clock

Adjust Date
: Set the current Date for the Clock

Snooze Time
: Set the global snooze time for all alarms. (Default 5 min)

Snooze Expire
: Set the time after which the clock gives up with snoozing and discards the alarm. (Default: 60 min)

Trigger Test Ring
: Will trigger a test-alarm so you know that your Beeper works.

## Pinout

The following table shows the default pinout, as defined in [Clock_v1_hardware.h](./Clock_v1_hardware.h), you can change most of those pins if your hardware requires it.

| ClockOS Name | Arduino Pin | Notes        |
|--------------|-------------|--------------|
| LCD_RS_PIN   | 4           |              |
| LCD_EN_PIN   | 2           |              |
| LCD_D4_PIN   | 3           |              |
| LCD_D5_PIN   | 6           |              |
| LCD_D6_PIN   | 7           |              |
| LCD_D7_PIN   | 8           |              |
| BTN_OK       | A1          |              |
| BTN_UP       | 12          |              |
| BTN_DOWN     | A2          |              |
| BTN_LEFT     | A3          |              |
| BTN_RIGHT    | A0          |              |
| LED_PIN      | 13          |              |
| RGB_R_PIN    | 5           | PWM Required |
| RGB_G_PIN    | 10          | PWM Required |
| RGB_B_PIN    | 9           | PWM Required |
| BEEPER_PIN   | 11          | PWM Required |
|              | 18 (A4)     | I²C SCL      |
|              | 19 (A5)     | I²C SDA      |
|              | 0           | Serial RX    |
|              | 1           | Serial TX    |

<b>Note:</b> Pins 3 and 11 are messed up by the [`tone()` function](https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/), rey to use one for the beeper and the other for non-PWM functionality.

## Serial Interface v2

The new Serial Interface (currently) accepts (and will in the future send) packet based messages that could be routed through multiple devices.

On the comms layer (roughly equivalent to OSI Layers 2 and 3) the packets get transferred from host to host using a 16-bit ID for addressing.

For ease of debugging big endian numbers are used (when you look at the hexdump the bytes are in the same order as you would write them out).

The header looks like this:
* 2-byte magic number `0xcc5a`
* 2-byte host ID
* 2-byte Time to live (packet won't get forwarded when it arrives with a ttl of 0, gets decremented for every hoo)
* 2-byte packet payload length+2 (2-byte checksum is mandated)
* 1-byte header checksum
* 1-byte bit inverted header checksum
* payload data
* 1-byte packet checksum
* 1-byte bit inverted packet checksum

The checksums are calculated using an 8-bit number and adding up the incoming bytes (overflow simply gets dropped), the checksum should make the counter `0`. The packet checksum includes the inverted header checksum.
Currently the serial interface ticks at 9600 baud.

## Compiling and Uploading with `arduino-cli`

```sh
arduino-cli compile --fqbn arduino:avr:uno --libraries libs/
arduino-cli upload --fqbn arduino:avr:uno -p /dev/tty<something>
```

The Arduino UNO Board maps to `/dev/ttyACM0` for me (voidlinux).
My FTDI breakout maps to `/dev/ttyUSB0`.

Use the `--log` function to make the arduino cli tell you what it is doing.

### Dependencies

Go through these if the first compilation fails.

You may have to install the `arduino:avr` core (the compile command will tell you) if you have never used arduino-cli with an avr controller (i.e. on a fresh install).

```sh
arduino-cli core install arduino:avr
```

The `RTClib` (Adafruit) and `LiquidCrystal` (Arduino) libraries are also required depedencies, you can install them using the following command.

```sh
arduino-cli lib install RTClib LiquidCrystal
```

## License

Copyright 2024 Slatian

Unless noted otherwise you can use the content of this repository under a [LGPL-3.0-only](./LICENSES/LGPL-3.0-only.txt) License.

The files under [libs/memory_free/](./libs/memory_free/) are [MIT Licensed](./LICENSES/MIT.txt) and Copyright by Michael P. Flaga. (Thank you!)

The custom icons for the LCD-Panel are released under [CC0-1.0](./LICENSES/CC0-1.0.txt) or Public Domain.

The digits for the "Big Clock" Clockface are taken or derived from the [small font used in the TIS 3D Minecraft Mod](https://raw.githubusercontent.com/MightyPirates/TIS-3D/1.20.4/common/src/main/resources/assets/tis3d/textures/font/small.png) which are [realeased into the Public Domain by notice in the README](https://github.com/MightyPirates/TIS-3D/tree/1.20.4?tab=readme-ov-file#license--use-in-modpacks). (Thanks Sangar!) Changed Digits are also Public Domain.

**Note:** The decompression algorithm implementation remains under the LGPL 3.0 only.
