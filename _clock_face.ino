void start_clock_face() {
	ui_mode = CLOCK;
	ui_clear();
	update_clock_face();
}

void update_clock_face() {
	if (clock_should_display_big_digits()) {
		start_big_clock();
	} else {
		ui_set_message(notification.message);
		display_time();
	}
}

void display_time() {
	char time_chars[17] = {};
	for (uint8_t i = 0; i<16 && setting_time_format[i]; i++) {
		time_chars[i] = setting_time_format[i];
		if (
			(theme_settings.flags & THEME_FLAG_BLINK_COLON) &&
			setting_time_format[i] == ':'
		) {
			if (now.second()%2) {
				time_chars[i] = ' ';
			}
		}
	}
	time_chars[16] = 0;
	ui_set_title(now.toString(time_chars));
}

void clock_face_handle_button(int button, uint8_t repeat_count) {
	switch (button) {
		case BTN_OK:
			dismiss_notification(RESPONSE_DISMISS);
			break;
		case BTN_DOWN:
			if (notification.mode != NO_NOTIFICATION) {
				dismiss_notification(RESPONSE_SNOOZE);
			} else {
				switch_ui_mode_to(ALARM_SELECTOR);
			}
			break;
		case BTN_UP:
			// reserved for command menu
			send_free_memory_notification();
			break;
		case BTN_LEFT:
			//unused for now
			break;
		case BTN_RIGHT:
			start_settings_menu();
			break;
	}
}

bool clock_should_display_big_digits() {
	if (theme_settings.flags & THEME_FLAG_DISABLE_BIG_CLOCK) {
		return false;
	}
	return !(notification.mode < NOTIFY_LOW || was_recently_active);
}
