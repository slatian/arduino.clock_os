#ifndef ui_h_INCLUDED
#define ui_h_INCLUDED

#include <LiquidCrystal.h>

// Save and scroll the current message
String ui_current_message = "";
unsigned long ui_current_message_start_time = 0;

enum ui_icon_t {
	ICON_NO_ICON,
	ICON_SWITCH_OFF,
	ICON_SWITCH_ON,
};

void ui_initalize();

void ui_clear();

void ui_update_message_scroll();

void ui_set_message(String message);
void ui_set_message(char *message);
void ui_set_message_with_icon(String message, ui_icon_t icon);
void ui_set_switch_message(String message, bool value);
void ui_clear_lcd_line();
void ui_set_title(String title);
void ui_set_menu_arrows(bool up, bool down);

#endif // ui_h_INCLUDED
