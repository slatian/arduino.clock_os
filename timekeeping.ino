/*
 * timekeeping.c ClockOS Timekeeping functionality, this file talks to the RTC
 */

#include "timekeeping.h"

// We use two RTCs, a software one as "cache" and a hardware one for correcting that cache
// This has the advantage that we can handle no harware RTC being available
RTC_DS1307 real_rtc;
RTC_Micros rtc;

// Tries to initalize a clock on the given I2C bus,
// Initialize after retrieving settings.
// returns true if the hardware RTC could be activated
// false if we are running on a software RTC only.
bool timekeepinng_initalize_clocks(TwoWire* wire) {
	// Initalize Software RTC and set the time to the compiletime
	rtc.begin(DateTime());
	// This way we're initalizing the rtc, the now and are automatically
	// summertime aware
	adjust_clocks(DateTime(F(__DATE__), F(__TIME__)));

	// Initalize our hardware RTC
	if (real_rtc.begin(wire)) {
		// The DS1307 may not be ticking right now because nobody set the time yet
		if (!real_rtc.isrunning()) {
			real_rtc.adjust(now);
		}
		delay(500);
		// Is it running now?
		real_rtc_active = real_rtc.isrunning();
	}

	if (real_rtc_active) {
		if (real_rtc.now() < now) {
			// RTC Time is less than compile time, this is probably an error, adjust clock
			real_rtc.adjust(now);
		} else {
			// RTC Time is after compile time, since it's our best guess, trust it
			rtc.adjust(real_rtc.now());
		}
		return true;
	}
	return false;
}

void timekeeping_update_now() {
	if (timekeeping_is_on_summertime) {
		now = rtc.now() + TimeSpan(3600);
	} else {
		now = rtc.now();
	}
}

void timekeeping_syncronize_rtc() {
	if (real_rtc_active)
		if (real_rtc.isrunning())
			rtc.adjust(real_rtc.now());
}

void adjust_clocks(DateTime new_now) {
	if (timekeeping_is_on_summertime) {
		new_now = new_now - TimeSpan(3600);
	}
	rtc.adjust(new_now);
	rtc_was_adjusted = true;
	if (real_rtc_active)
		real_rtc.adjust(new_now);
	now = rtc.now();
}


