
/*
 * ClocksOS file format structs.
 */

#ifndef file_h_INCLUDED
#define file_h_INCLUDED

#define STORAGE_EEPROM 0

#define DDT_CLOCK_SETTINGS 0x10
#define DDT_ALARM 0x11
#define DDT_THEME_SETTINGS 0x12

struct clock_settings_file {
	uint8_t version;
	uint8_t summertime;
	uint16_t minutes_snooze_time;
	uint16_t give_up_after_aggregate_snooze_time_of;
};

#define THEME_FLAG_BLINK_COLON 0x01
#define THEME_FLAG_DISABLE_BIG_CLOCK 0x02

struct theme_settings_file {
	uint8_t version;
	uint8_t flags;
	color_t color_backlight;
	color_t color_notification;
};

#endif // file_h_INCLUDED
