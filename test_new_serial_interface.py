import serial
import struct
import time
from datetime import datetime,timezone
import sys
from dateutil.tz import tzlocal

def pack_up(payload):
	if len(payload) > 0xFF:
		raise Exception("payload is too long, max length is 0xFF")
	header = struct.pack("bb", 0, len(payload))
	return header + payload 


def read_packet(line):
	# wait for nonzero
	while True:
		if line.read(size=1)[0] == 0:
			break
	# wait for length
	length = 0
	while length == 0:
		length = line.read(size=1)[0]
		if length == 0:
			line.write(b'\0\0')
	return line.read(size=length)

def stringify_packet(data):
	ptype = data[0]
	data = data[1:]
	match ptype:
		case 16:
			return print_time_packet(data)
		case _:
			return f"Unknown protocol {ptype}: {data}"

def print_time_packet(data):
	output = "RTC Exchange: "
	match data[0]:
		case 0:
			output += "REQUEST"
		case 1:
			output += "ANNOUNCE"
		case 2:
			output += "SET"
		case _:
			output += f"UNKNOWN({data[0]}) "
	if len(data) >= 10:
		verb,flags,timestamp = struct.unpack("<bbq",data)
		date = datetime.utcfromtimestamp(timestamp)
		output += f" flags: {flags} time: {date}"
	else:
		output += " (No useable extra data.)"
	return output
	
print("Connecting ...")
line = serial.Serial("/dev/ttyUSB0", 9600)
 
print("Waiting for restart ...")
time.sleep(3)
 
print("Sending Test packet ...")
tz = tzlocal()
now = datetime.now(tz)
unix_now = int(now.timestamp()+(tz.utcoffset(now).seconds))
line.write(pack_up(struct.pack("<bbbq",16,2,0,unix_now)))
#line.write(pack_up(b'\x10\x00'))
print("Reply:")
print(stringify_packet(read_packet(line)))
print("Done!")
