
#pragma once

enum dman_t : byte {
	DMAN_REQUEST = 0,
	DMAN_ANNOUNCE = 1,
	DMAN_SET = 2,
	DMAN_PUT = 3,
};

#define SERIAL_PROTOCOL_RTC_EXCHANGE 16
#define SERIAL_PROTOCOL_NOTIFICATION 17

#define RTC_EXCHANGE_IS_BATTERY_BACKED 1
#define RTC_EXCHANGE_WAS_ADJUSTED 2
#define RTC_EXCHANGE_INCLUDES_ACCURATE_TIMEZONE 4

struct rtc_exchange_protocol_data_t {
	dman_t control = 0;
	byte flags = 0;
	long long timestamp = 0;
	unsigned long timezone_offset = 0;
};

void handle_rtc_exchange_incoming(rtc_exchange_protocol_data_t *data, byte packet_length);

struct notification_exchange_protocol_data_t {
	dman_t control = 0;
	char action = 0;
	byte severity;
	unsigned long serial;
	unsigned long cookie;
	long long timestamp_sent;
	long long timestamp_expire;
	byte message_size;
};

void handle_notification_exchange_incoming(notification_exchange_protocol_data_t *data, byte packet_length);

