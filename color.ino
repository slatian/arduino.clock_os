#include "color.h"

color_t u32_to_color(uint32_t c) {
	return color_t {
		r: c >> 16 & 0x000000ff,
		g: c >>  8 & 0x000000ff,
		b: c       & 0x000000ff
	};
}
