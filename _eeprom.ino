/*
	EEPROM Module for Clock OS Verion 1

	Provides:
	* bool restore_from_eeprom()
		Restores clock settings from the EEPROM, returns true on successs
	* void save_to_eeprom(bool force_save = false)
		Saves clock settings to the EEPROM if the flag for unsaved data has been set or if force_save is set
	* void flag_unsaved_data()
		A global flag that can be set when there is new data for being saved
		
	Needs:
	* A defined `N_ALARMS` and an `alarms[N_ALARMS]` array
	* A Microcontroller with enough EEPROM
*/

#include <EEPROM.h>

#include <one_block_fs.h>

#include "storage.h"

// Save only when neccessary
bool has_unsaved_data = false;

#define SLATECAVE_CLOCK_V1_FIXED_ALARM_OFFSET 4
#define SLATECAVE_CLOCK_V2_FIXED_SETTIGS_OFFSET 302

inline void flag_unsaved_data() {
	has_unsaved_data = true;
}

inline bool is_unsaved_data_flag() {
	return has_unsaved_data;
}

bool restore_from_eeprom() {
	//Test if there is somwthing we can restore
	if (!(EEPROM[0] == 0x5c && EEPROM[1] == 0xc1 && EEPROM[2] == 0x0c)) {
		return false;
	}
	uint8_t version = EEPROM[3];
	// Read Alarms
	if (version >= 1 && version <= 3) {
		for (int i=0; i < N_ALARMS; i++) {
			EEPROM.get(
				SLATECAVE_CLOCK_V1_FIXED_ALARM_OFFSET+(i*3),
				alarms[i].settings.flags
			);
			EEPROM.get(
				SLATECAVE_CLOCK_V1_FIXED_ALARM_OFFSET+(i*3)+1,
				alarms[i].settings.minutes_after_midnight
			);
		}
	}
	// Read settings
	if (version >= 2 && version <= 3) {
		EEPROM.get(
			SLATECAVE_CLOCK_V2_FIXED_SETTIGS_OFFSET,
			setting_minutes_snooze_time
		);
		EEPROM.get(
			SLATECAVE_CLOCK_V2_FIXED_SETTIGS_OFFSET+4,
			setting_give_up_after_aggregate_snooze_time_of
		);
	}
	if (version == 3) {
		EEPROM.get(
			SLATECAVE_CLOCK_V2_FIXED_SETTIGS_OFFSET+8,
			timekeeping_is_on_summertime
		);
	}
	if (version == 4) {
		uint8_t descriptor_id;
		obfs_data_descriptor_t descriptor;
		// read clock settings
		obfs_res error = obfs_find_descriptor_of_type(
			STORAGE_EEPROM,
			&descriptor_id,
			&descriptor,
			0xff,
			DDT_CLOCK_SETTINGS,
			true
		);
		if (!error) {
			clock_settings_file settings_file;
			error = obfs_descriptor_read_data(
				STORAGE_EEPROM,
				&descriptor,
				(uint8_t*) &settings_file,
				sizeof(clock_settings_file),
				0
			);
			if (!error) {
				timekeeping_is_on_summertime = settings_file.summertime;
				setting_minutes_snooze_time = settings_file.minutes_snooze_time;
				setting_give_up_after_aggregate_snooze_time_of =
					settings_file.give_up_after_aggregate_snooze_time_of;
			}
		}
		// read alarm settings
		for (uint8_t i = 0; i<N_ALARMS; i++) {
			error = obfs_find_descriptor_of_type(
				STORAGE_EEPROM,
				&descriptor_id,
				&descriptor,
				0xff,
				DDT_ALARM,
				i == 0
			);
			if (error) { break; }
			obfs_descriptor_read_data(
				STORAGE_EEPROM,
				&descriptor,
				(uint8_t*) &alarms[i].settings,
				sizeof(alarm_settings_t),
				0
			);
			if (!error) {
				alarms[i].drive = STORAGE_EEPROM;
				alarms[i].descriptor_id = descriptor_id;
				alarms[i].is_in_storage = true;
			}
		}
		// Read Theme settings
		error = obfs_find_descriptor_of_type(
			STORAGE_EEPROM,
			&descriptor_id,
			&descriptor,
			0xff,
			DDT_THEME_SETTINGS,
			true
		);
		if (!error) {
			error = obfs_descriptor_read_data(
				STORAGE_EEPROM,
				&descriptor,
				(uint8_t*) &theme_settings,
				sizeof(theme_settings_file),
				0
			);
		}
		
	}
	return true;
}

obfs_res find_or_allocate_singleton_file_of_type(
	uint8_t drive,
	uint8_t type,
	uint16_t size,
	uint8_t* descriptor_id,
	obfs_data_descriptor_t* descriptor
);

void format_eeprom() {
	obfs_res error;
	//save signature
	EEPROM.update(0, 0x5c); //SlateCave
	EEPROM.update(1, 0xc1); //CLOCk
	EEPROM.update(2, 0x0c);
	EEPROM.update(3, 0x04); //v4
	uint16_t size = obfs_storage_get_size(STORAGE_EEPROM);
	size -= (sizeof(obfs_header_t)+sizeof(obfs_data_descriptor_t));
	uint8_t num_of_descriptors = size/(10+sizeof(obfs_data_descriptor_t));

	error = obfs_format_fs(STORAGE_EEPROM, num_of_descriptors);
	if (error) {
		return;
	}
	dump_eeprom();

}


void save_to_eeprom(bool force_save) {
	if (has_unsaved_data || force_save) {
		uint8_t magic_1 = EEPROM[0];
		uint8_t magic_2 = EEPROM[1];
		uint8_t magic_3 = EEPROM[2];
		uint8_t version = EEPROM[3];
		obfs_res error;
		if (magic_1 != 0x5c || magic_2 != 0xc1 || magic_3 != 0x0c || version != 4) {
			format_eeprom();
		}

		uint8_t descriptor_id;
		obfs_data_descriptor_t descriptor;
		// save basic clock settings
		error = find_or_allocate_singleton_file_of_type(
			STORAGE_EEPROM,
			DDT_CLOCK_SETTINGS,
			sizeof(clock_settings_file),
			&descriptor_id,
			&descriptor
		);
		if (!error) {
			clock_settings_file file = {
				version: 0,
				summertime: timekeeping_is_on_summertime,
				minutes_snooze_time: setting_minutes_snooze_time,
				give_up_after_aggregate_snooze_time_of:
					setting_give_up_after_aggregate_snooze_time_of,
			};
			obfs_descriptor_write_data(
				STORAGE_EEPROM,
				&descriptor,
				(uint8_t*) &file,
				sizeof(clock_settings_file),
				0
			);
		}
		// save alarms
		uint8_t i = 0;
		while (i<N_ALARMS) {
			error = obfs_find_descriptor_of_type(
				STORAGE_EEPROM,
				&descriptor_id,
				&descriptor,
				0xff,
				DDT_ALARM,
				i == 0
			);
			if (error) { break; }
			if (descriptor.size == sizeof(alarm_settings_t)) {
				obfs_descriptor_write_data(
					STORAGE_EEPROM,
					&descriptor,
					(uint8_t*) &alarms[i].settings,
					sizeof(clock_settings_file),
					0
				);
				i++;
			} else {
				obfs_free_descriptor(
					STORAGE_EEPROM,
					descriptor_id,
					false
				);
			}
		}
		for (;i<N_ALARMS;i++) {
			error = obfs_allocate_descriptor(
				STORAGE_EEPROM,
				&descriptor_id,
				&descriptor,
				sizeof(alarm_settings_t),
				DDT_ALARM
			);
			obfs_descriptor_write_data(
				STORAGE_EEPROM,
				&descriptor,
				(uint8_t*) &alarms[i].settings,
				sizeof(clock_settings_file),
				0
			);
		}
		// save theme settings
		error = find_or_allocate_singleton_file_of_type(
			STORAGE_EEPROM,
			DDT_THEME_SETTINGS,
			sizeof(theme_settings_file),
			&descriptor_id,
			&descriptor
		);
		if (!error) {
			obfs_descriptor_write_data(
				STORAGE_EEPROM,
				&descriptor,
				(uint8_t*) &theme_settings,
				sizeof(theme_settings_file),
				0
			);
		}
		
		has_unsaved_data = false;
	}
}

void dump_eeprom() {
	Serial.println(F("// EEPROM dump start:"));
	int size = EEPROM.length();
	for (int i = 0; i<size; i++) {
		uint8_t b = EEPROM[i];
		Serial.print(b>>4, HEX);
		Serial.print(b&0x0F, HEX);
		if (i%16 == 15) {
			Serial.println("");
		} else {
			Serial.print(" ");
		}
	}
	Serial.println(F("// EEPROM dump end."));
}

obfs_res find_or_allocate_singleton_file_of_type(
	uint8_t drive,
	uint8_t type,
	uint16_t size,
	uint8_t* descriptor_id,
	obfs_data_descriptor_t* descriptor
) {
	bool is_first_iteration = true;
	obfs_res error = OBFS_OK;
	while (error == OBFS_OK) {
		error = obfs_find_descriptor_of_type(
			drive,
			descriptor_id,
			descriptor,
			0xff,
			type,
			is_first_iteration
		);
		if (!error) {
			if (descriptor->size == size) {
				return OBFS_OK;
			} else {
				// Discard descriptors of the wrong size.
				obfs_free_descriptor(drive, *descriptor_id, false);
			}
		}
	}
	// Not found allocate a fresh one …
	return obfs_allocate_descriptor(
		drive,
		descriptor_id,
		descriptor,
		size,
		type
	);
}

uint16_t obfs_storage_get_size(uint8_t drive) {
	switch (drive) {
		case STORAGE_EEPROM:
			return max(1000, EEPROM.length()-24);
		default:
			return 0;
	}
}

obfs_res obfs_storage_read_data(
	uint8_t drive,
	uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
) {
	if (drive == STORAGE_EEPROM) {
		for (unsigned int i = 0; i<size; i++) {
			buffer[i] = EEPROM.read(at_offset+i+24);
		}
		return OBFS_OK;
	}
	return OBFS_STORAGE_ERROR_UNKNOWN_DRIVE;
}

obfs_res obfs_storage_write_data(
	uint8_t drive,
	const uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
) {
	if (drive == STORAGE_EEPROM) {
		for (unsigned int i = 0; i<size; i++) {
			EEPROM.update(at_offset+i+24, buffer[i]);
		}
		return OBFS_OK;
	}
	return OBFS_STORAGE_ERROR_UNKNOWN_DRIVE;
}

