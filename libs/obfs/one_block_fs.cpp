// Copyright 2024 Slatian <baschdel@disroot.org>
// SPDX-FileContributor: Slatian
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "one_block_fs.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/*
 * One Block FS library file.
 */

/* Private functions */

obfs_res obfs_test_if_operation_matches_storage(
	uint8_t drive,
	uint16_t size,
	uint16_t at_offset
) {
	uint16_t drive_size = obfs_storage_get_size(drive);

	if (drive_size == 0) {
		return OBFS_STORAGE_ERROR_UNKNOWN_DRIVE;
	}

	// Size check and overflow check
	if (at_offset+size > drive_size || at_offset+size < at_offset) {
		return OBFS_STORAGE_ERROR_OUT_OF_BOUNDS;
	}

	return OBFS_OK;
}

obfs_res obfs_storage_read_data_checked(
	uint8_t drive,
	uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
) {
	obfs_res error = obfs_test_if_operation_matches_storage(
		drive, size, at_offset
	);
	if (error) {
		return error;
	}
	return obfs_storage_read_data(drive, buffer, size, at_offset);
}

obfs_res obfs_storage_write_data_checked(
	uint8_t drive,
	const uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
) {
	obfs_res error = obfs_test_if_operation_matches_storage(
		drive, size, at_offset
	);
	if (error) {
		return error;
	}
	return obfs_storage_write_data(drive, buffer, size, at_offset);
}

void format_descriptor_section(uint8_t drive, uint16_t offset, uint16_t size) {
	uint8_t desc_type = OBFS_DDT_UNUSED_DESCRIPTOR;
	while (size > DATA_DESCRIPTOR_SIZE) {
		obfs_storage_write_data_checked(drive, &desc_type, 1, offset);
		offset += DATA_DESCRIPTOR_SIZE;
		size -= DATA_DESCRIPTOR_SIZE;
	}
}

obfs_res obfs_update_data_descriptor(
	uint8_t drive,
	uint8_t descriptor_id,
	const obfs_data_descriptor_t* desc
) {
	uint16_t size = obfs_storage_get_size(drive);
	uint8_t num_descriptors = obfs_get_number_of_file_descriptors(drive);
	if (size > 0 && num_descriptors > descriptor_id) {
		return obfs_storage_write_data_checked(
			drive,
			(uint8_t*) desc,
			DATA_DESCRIPTOR_SIZE,
			size-(DATA_DESCRIPTOR_SIZE*descriptor_id)-DATA_DESCRIPTOR_SIZE
		);
	}
	return OBFS_ERROR;
}


/* Public functions */

uint8_t obfs_get_number_of_file_descriptors(uint8_t drive) {
	uint8_t num_of_descriptors = 0;
	if (obfs_storage_read_data_checked(drive, &num_of_descriptors, 1, 4)) {
		return 0;
	}
	return num_of_descriptors;
}

obfs_res obfs_read_data_descriptor(
	uint8_t drive,
	uint8_t descriptor_id,
	obfs_data_descriptor_t* desc
) {
	uint16_t size = obfs_storage_get_size(drive);
	uint8_t num_descriptors = obfs_get_number_of_file_descriptors(drive);
	if (size > 0 && (num_descriptors > descriptor_id)) {
		return obfs_storage_read_data_checked(
			drive,
			(uint8_t*) desc, 
			DATA_DESCRIPTOR_SIZE,
			size-(DATA_DESCRIPTOR_SIZE*descriptor_id)-DATA_DESCRIPTOR_SIZE
		);
	}
	return OBFS_ERROR;

}

// The descriptor pointer will be used for iterating, assume it contains invalid data
// when function returns false.
obfs_res obfs_find_free_space(
	uint8_t drive,
	uint8_t* descriptor_id,
	obfs_data_descriptor_t* descriptor,
	uint16_t size
) {
	uint8_t best_descriptor = 0;
	uint16_t best_descriptor_size = 0;
	descriptor->next_descriptor = 0;
	uint8_t current_descriptor = 0;
	for (uint8_t i = 0xff; i > 0; i--) {
		current_descriptor = descriptor->next_descriptor;
		// break if we can't read a descriptor
		if (obfs_read_data_descriptor(drive, descriptor->next_descriptor, descriptor)) {
			break;
		}
		if (descriptor->type == OBFS_DDT_EMPTY) {
			if (descriptor->size >= size) {
				if (best_descriptor_size == 0 || descriptor->size < best_descriptor_size) {
					best_descriptor_size = descriptor->size;
					best_descriptor = current_descriptor;
					if (descriptor->size == size) {
						break;
					}
				}
			}
		}
		// break if this was the last descriptor
		if (descriptor->next_descriptor == 0) {
			break;
		}
	}
	if (best_descriptor_size) {
		*descriptor_id = best_descriptor;
		return OBFS_OK;
	}
	return OBFS_ERROR;
}

// result gets written to desc, calling again (with first_call = false)
// will return the next matching descriptor.
// neither descriptor_id, nor descriptor may be null.
// descriptor will be overwritten.
obfs_res obfs_find_descriptor_of_type(
	uint8_t drive,
	uint8_t* descriptor_id,
	obfs_data_descriptor_t* descriptor,
	uint8_t type_mask,
	uint8_t type,
	bool first_call
) {
	uint8_t num_of_descriptors = obfs_get_number_of_file_descriptors(drive);
	
	uint8_t i = 0;
	if (!first_call) {
		i = *descriptor_id;
		i++;
	}
	for (; i<num_of_descriptors; i++) {
		if (obfs_read_data_descriptor(drive, i, descriptor)) {
			return OBFS_FIND_DESCRIPTOR_OF_TYPE_ERROR_UNABLE_TO_READ_DESCRIPTOR;
		}
		if ((descriptor->type & type_mask) == type) {
			*descriptor_id = i;
			return OBFS_OK;
		}
	}
	return OBFS_FIND_DESCRIPTOR_OF_TYPE_NOT_FOUND;
}

obfs_res obfs_allocate_descriptor(
	uint8_t drive,
	uint8_t* new_descriptor_id,
	obfs_data_descriptor_t* new_descriptor,
	uint16_t size,
	uint8_t type
) {
	if (obfs_find_free_space(drive, new_descriptor_id, new_descriptor, size)) {
		return OBFS_ALLOCATE_ERROR_NO_FREE_SPACE_FOUND;
	}

	if (new_descriptor->size > size) {
		// Grab an unused descriptor and allocate it the empty space
		uint8_t unused_descriptor_id;
		obfs_data_descriptor_t unused_descriptor;

		if (obfs_find_descriptor_of_type(
			drive,
			&unused_descriptor_id, &unused_descriptor,
			0xff, OBFS_DDT_UNUSED_DESCRIPTOR,
			true)
		) {
			return OBFS_ALLOCATE_ERROR_NO_UNUSED_DESCRIPTOR_FOUND;
		}
		unused_descriptor.type = OBFS_DDT_EMPTY;
		unused_descriptor.size = new_descriptor->size - size;
		unused_descriptor.offset = new_descriptor->offset + size;
		unused_descriptor.next_descriptor = new_descriptor->next_descriptor;
		new_descriptor->next_descriptor = unused_descriptor_id;

		if (obfs_update_data_descriptor(drive, unused_descriptor_id, &unused_descriptor)) {
			return OBFS_ALLOCATE_ERROR_UPDATNG_LEFTOVERS_FAILED;
		}
	}

	new_descriptor->size = size;
	new_descriptor->type = type;
	
	if (obfs_update_data_descriptor(drive, *new_descriptor_id, new_descriptor)) {
		return OBFS_ALLOCATE_ERROR_UPDATING_NEW_FAILED;
	};

	return OBFS_OK;

}

obfs_res obfs_free_descriptor(
	uint8_t drive,
	uint8_t descriptor_id,
	bool clear
) {
	uint8_t num_of_descriptors = obfs_get_number_of_file_descriptors(drive);
	
	obfs_data_descriptor_t descriptor;
	if (obfs_read_data_descriptor(drive, descriptor_id, &descriptor)) {
		return OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_READ_DESCRIPTOR;
	}

	if (descriptor.type == OBFS_DDT_EMPTY || descriptor.type == OBFS_DDT_UNUSED_DESCRIPTOR) {
		return OBFS_FREE_DESCRIPTOR_ERROR_DESCRIPTOR_EMPTY_OR_UNUSED;
	}

	if (clear) {
		obfs_storage_write_data_checked(drive, NULL, descriptor.size, descriptor.offset);
	}

	obfs_data_descriptor_t previous_descriptor;
	uint8_t previous_descriptor_id = 0;
	bool has_relevant_previous_descriptor = false;
	obfs_data_descriptor_t next_descriptor;
	uint8_t next_descriptor_id = descriptor.next_descriptor;
	bool has_relevant_next_descriptor = false;

	if (descriptor_id > 0) {
		// Try to find a previous descriptor if we aren't trying to free the first one.
		for (uint8_t i = 0; i<num_of_descriptors; i++) {
			if (obfs_read_data_descriptor(drive, i, &previous_descriptor)) {
				return OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_READ_POTENTIAL_PREVIOUS_DESCTIPTOR;
			}
			if (previous_descriptor.next_descriptor == descriptor_id) {
				if (previous_descriptor.type == OBFS_DDT_EMPTY) {
					has_relevant_previous_descriptor = true;
					previous_descriptor_id = i;
					break;
				}
			}
		}
	}

	if (next_descriptor_id) {
		if (obfs_read_data_descriptor(drive, next_descriptor_id, &next_descriptor)) {
			return OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_READ_NEXT_DESCTIPTOR;
		}
		if (next_descriptor.type == OBFS_DDT_EMPTY) {
			has_relevant_next_descriptor = true;
		}
	}

	uint16_t size = descriptor.size;
	uint8_t new_next_descriptor_id = descriptor.next_descriptor;

	if (has_relevant_next_descriptor) {
		next_descriptor.type = OBFS_DDT_UNUSED_DESCRIPTOR;
		size += next_descriptor.size;
		new_next_descriptor_id = next_descriptor.next_descriptor;
	}

	if (has_relevant_previous_descriptor) {
		descriptor.type = OBFS_DDT_UNUSED_DESCRIPTOR;
		previous_descriptor.next_descriptor = new_next_descriptor_id;
		previous_descriptor.size += size;

		if (obfs_update_data_descriptor(
			drive,
			previous_descriptor_id,
			&previous_descriptor)
		) {
			return OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_UPDATE_PREVIOUS_DESCRIPTOR;
		}
		
	} else {
		descriptor.type = OBFS_DDT_EMPTY;
		descriptor.next_descriptor = new_next_descriptor_id;
		descriptor.size = size;
	}
	if (obfs_update_data_descriptor(drive, descriptor_id, &descriptor)) {
		return OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_UPDATE_DESCRIPTOR;
	}
	if (has_relevant_next_descriptor) {
		if (obfs_update_data_descriptor(drive, next_descriptor_id, &next_descriptor)) {
			return OBFS_FREE_DESCRIPTOR_ERROR_UNABLE_TO_UPDATE_NEXT_DESCRIPTOR;
		}
	}
	return OBFS_OK;
}

obfs_res obfs_descriptor_read_data(
	uint8_t drive,
	const obfs_data_descriptor_t* descriptor,
	uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
) {
	// overflow check
	if (at_offset+size < at_offset) {
		return OBFS_DESCRIPTOR_IO_ERROR_OUT_OF_BOUNDS;
	}

	// actual size check
	if (at_offset+size > descriptor->size) {
		return OBFS_DESCRIPTOR_IO_ERROR_OUT_OF_BOUNDS;
	}

	return obfs_storage_read_data_checked(drive, buffer, size, descriptor->offset+at_offset);
}

obfs_res obfs_descriptor_write_data(
	uint8_t drive,
	const obfs_data_descriptor_t* descriptor,
	const uint8_t* buffer,
	uint16_t size,
	uint16_t at_offset
) {
	// overflow check
	if (at_offset+size < at_offset) {
		return OBFS_DESCRIPTOR_IO_ERROR_OUT_OF_BOUNDS;
	}

	// actual size check
	if (at_offset+size > descriptor->size) {
		return OBFS_DESCRIPTOR_IO_ERROR_OUT_OF_BOUNDS;
	}

	return obfs_storage_write_data_checked(drive, buffer, size, descriptor->offset+at_offset);

}

// Returns a nonzero error code on failure
obfs_res obfs_format_fs(uint8_t drive, uint8_t num_of_descriptors) {
	uint16_t size = obfs_storage_get_size(drive);
	uint16_t inital_descriptors_size = DATA_DESCRIPTOR_SIZE*num_of_descriptors;
	obfs_res error = OBFS_OK;

	// We need at least 2 descriptors
	if (num_of_descriptors < 2) {
		return OBFS_FORMAT_FS_ERROR_NOT_ENOUGH_DESCRIPTORS;
	}

	if (inital_descriptors_size + sizeof(obfs_header_t) > size) {
		return OBFS_FORMAT_FS_ERROR_WAY_TOO_MANY_DESCRIPTORS;
	}

	obfs_header_t header = {
		magic_1: OBFS_MAGIC_1,
		magic_2: OBFS_MAGIC_2,
		version: 1,
		header_size: sizeof(obfs_header_t),
		num_descriptors: num_of_descriptors,
		reserved: 0,
	};

	// Set number of descriptors
	if (obfs_storage_write_data_checked(drive, (uint8_t*) &header, header.header_size, 0)) {
		return OBFS_FORMAT_FS_ERROR_HEADER_WRITE_FAIL;
	}

	// id: 0
	obfs_data_descriptor_t empty_sector = {
		type: OBFS_DDT_EMPTY,
		next_descriptor: 0,
		size: (uint16_t) (size - inital_descriptors_size - header.header_size),
		offset: header.header_size,
	};
	obfs_data_descriptor_t unused_descriptor = {
		type: OBFS_DDT_UNUSED_DESCRIPTOR,
		next_descriptor: 0,
		size: 0,
		offset: 0,
	};

	if (obfs_update_data_descriptor(drive, 0, &empty_sector)) {
		return OBFS_FORMAT_FS_ERROR_EMPTY_SECTOR_WRITE_FAIL;
	}
	for (uint8_t i=1; i<num_of_descriptors; i++) {
		if (obfs_update_data_descriptor(drive, i, &unused_descriptor)) {
			return OBFS_FORMAT_FS_ERROR_DESCRIPTOR_INIT_WRITE_FAIL;
		}
	}
	return OBFS_OK;
}

