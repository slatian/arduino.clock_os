/*
	Time Editor for Clock OS Verion 1

	This time editor can be used for adjusting a clock or for setting an alarm.
	It was made for a 16x2 Caracter display and a mini 5 button joystick

	Provides:
	* start_time_editor(ui_mode_t mode, time_editor_save_action_t save_action)
		Starts the time editor,
		possible modes are DATE_EDITOR and TIME_EDITOR
		possible save_actions are ADJUST and SET_ALARM
	* edit_time_handle_button(int button, bool repeat)
		Handles input for the editor ui
	* update_time_editor_visuals()
		Redraw the wime editor
	* time_editor_load_current_date_time()
		Load the current time from the RTC

	Needs:
	* A 16x2 caracter LCD obkect called `lcd`
	* External definitions of the ui_mode_t and time_editor_save_action_t enums
	* An `alarms` array and a `currently_selected_alarm` index (for the SET_ALARM)
	* A global `DateTime now` object
	* An `adjust_clocks(DateTime new_now)` function
*/

#import <RTClib.h>
#import "alarm.h"
 
uint16_t year = 0;
uint8_t month = 0;
uint8_t day = 0;
uint8_t hour = 0;
uint8_t minute = 0;

byte time_editor_cursor = 0;

// No PROGMEM here as it somehow causes a nasty bug in the day caclculation below
const byte days_per_month[] = {31,28,31,30,31,30,31,31,30,31,30,31};

void start_time_editor(ui_mode_t mode, time_editor_save_action_t save_action){
	if (mode != DATE_EDITOR && mode != TIME_EDITOR) { return; }
	lcd.cursor();
	lcd.blink();
	ui_set_message("");
	time_editor_cursor = 0;
	time_editor_save_action = save_action;
	ui_mode = mode;
	switch (save_action) {
		case ADJUST:
			time_editor_load_current_date_time();
			ui_set_title((ui_mode==DATE_EDITOR)?"Adjust Date:":"Adjust Time:");
			break;
		case SET_ALARM:
			int minutes_after_midnight = alarms[currently_selected_alarm].settings.minutes_after_midnight;
			hour = minutes_after_midnight/60;
			minute = minutes_after_midnight%60;
		default:
			break;
	}
	update_time_editor_visuals();
}

void edit_time_handle_button(int button, bool repeat) {
	int edit_direction = 0;
	switch(button) {
		case BTN_LEFT:
			if (time_editor_cursor > 0) {
				time_editor_cursor--;
			} else if (repeat) {
				//cancel
				ui_go_back();
				return;
			}
			break;
		case BTN_RIGHT:
			if (time_editor_cursor < ui_mode==DATE_EDITOR?2:1) {
				time_editor_cursor++;
			}
			break;
		case BTN_UP:
			edit_direction = +1;
			break;
		case BTN_DOWN:
			edit_direction = -1;
			break;
		case BTN_OK:
			switch (time_editor_save_action) {
				case ADJUST:
					if (ui_mode == DATE_EDITOR) {
						time_editor_adjust_date();
					} else {
						time_editor_adjust_time();
					}
					break;
				case SET_ALARM:
					alarms[currently_selected_alarm].settings.minutes_after_midnight = hour*60+minute;
					break;
			}
			ui_go_back();
			return;
	}
	if (ui_mode == DATE_EDITOR) {
		switch (time_editor_cursor) {
			case 0: year += edit_direction; break;
			case 1: month += edit_direction; break;
			case 2: day +=edit_direction; break;
		}
	} else {
		switch (time_editor_cursor) {
			case 0: hour += edit_direction; break;
			case 1: minute += edit_direction; break;
		}
	}
	year = constrain(year,2000,3000);
	month = constrain(month,1,12);
	byte days_per_this_month = days_per_month[month-1];
	if (days_per_this_month == 28 && year%4 == 0) days_per_this_month++;
	day = constrain(day,1,days_per_this_month);

	// a modulus wouldn't handle underflows very well
	if (hour > 250) {
		hour = 0;
	} else if (hour > 23) {
		hour = 23;
	}
	if (minute > 250) {
		minute = 0;
	} else if (minute > 59) {
		minute = 59;
	}
	
	update_time_editor_visuals();
}

const char time_editor_format[] = "hh:mm";
const char date_editor_format[] = "YYYY-MM-DD";

//offset by 3 to get time cursors
const byte date_time_editor_cursor_positions[] = {0,5,8,0,3};

void update_time_editor_visuals() {
	ui_clear_lcd_line(1);
	lcd.setCursor(0,1);
	char time_chars[17] = {};
	char* format = (ui_mode == DATE_EDITOR)?date_editor_format:time_editor_format;
	for (char i=0; i<16 && format[i]; i++) {
		time_chars[i] = format[i];
	}
	time_chars[16] = 0;
	lcd.print(DateTime(year, month, day, hour, minute, 0).toString(time_chars));
	lcd.setCursor(date_time_editor_cursor_positions[(ui_mode == DATE_EDITOR?0:3)+(time_editor_cursor)],1);
}


void time_editor_load_current_date_time() {
	year = now.year();
	month = now.month();
	day = now.day();
	hour = now.hour();
	minute = now.minute();
}

void time_editor_adjust_time() {
	adjust_clocks(DateTime(now.year(), now.month(), now.day(), hour, minute, 0));
}

void time_editor_adjust_date() {
	adjust_clocks(DateTime(year, month, day, now.hour(), now.minute(), now.second()));
}

