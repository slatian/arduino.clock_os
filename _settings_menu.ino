
enum settings_index_t: uint8_t {
	SETTING_TIME = 0,
	SETTING_DATE,
	SETTING_SUMMERTIME,
	SETTING_SNOOZE_TIME,
	SETTING_SNOOZE_MAX,
	SETTING_BLINK_COLON,
	SETTING_ENABLE_BIG_CLOCK,
	SETTING_BACKLIGHT_COLOR,
	SETTING_NOTIFICATION_COLOR,
	SETTING_TEST_RING,
	SETTING_DUMP_EEPROM,
	SETTING_FORMAT_EEPROM,
	SETTING_END_OF_MENU,
};

uint8_t settings_index = 0;

void start_settings_menu() {
	ui_mode = SETTINGS_MENU;
	settings_index = 0;
	update_settings_visuals(true);
}

void update_settings_visuals(bool initalize) {
	if (initalize) {
		ui_clear();
		ui_set_title(F("Settings:"));
	}
	ui_set_menu_arrows(settings_index, settings_index+1 < SETTING_END_OF_MENU);
	switch (settings_index) {
		case SETTING_TIME:
			ui_set_message(F("Adjust Time"));
			break;
		case SETTING_DATE:
			ui_set_message(F("Adjust Date"));
			break;
		case SETTING_SUMMERTIME:
			ui_set_switch_message(
				F("Summertime"),
				timekeeping_is_on_summertime
			);
			break;
		case SETTING_SNOOZE_TIME:
			ui_set_message(F("Snooze Time"));
			break;
		case SETTING_SNOOZE_MAX:
			ui_set_message(F("Snooze Expire"));
			break;
		case SETTING_BLINK_COLON:
			ui_set_switch_message(
				F("Animate Colon"),
				theme_settings.flags & THEME_FLAG_BLINK_COLON
			);
			break;
		case SETTING_ENABLE_BIG_CLOCK:
			ui_set_switch_message(
				F("Big Clock on Lights off"),
				!(theme_settings.flags & THEME_FLAG_DISABLE_BIG_CLOCK)
			);
			break;
		case SETTING_BACKLIGHT_COLOR:
			ui_set_message(F("Backlight Color"));
			break;
		case SETTING_NOTIFICATION_COLOR:
			ui_set_message(F("Notify Color"));
			break;
		case SETTING_TEST_RING:
			ui_set_message(F("Trigger Test Ring"));
			break;
		case SETTING_DUMP_EEPROM:
			ui_set_message(F("Dump EEPROM to Serial"));
			break;
		case SETTING_FORMAT_EEPROM:
			ui_set_message(F("Format EEPROM"));
			break;
	}
}

void settings_menu_handle_button(int button) {
	switch(button) {
		case BTN_LEFT:
			start_clock_face();
			return;
		case BTN_RIGHT:
		case BTN_OK:
			switch(settings_index) {
				case SETTING_TIME:
					start_time_editor(TIME_EDITOR, ADJUST);
					break;
				case SETTING_DATE:
					start_time_editor(DATE_EDITOR, ADJUST);
					break;
				case SETTING_SUMMERTIME:
					timekeeping_is_on_summertime = !timekeeping_is_on_summertime;
					has_unsaved_data = true;
					timekeeping_update_now();
					update_settings_visuals(false);
					break;
				case SETTING_SNOOZE_TIME:
					start_number_editor(
						setting_minutes_snooze_time,
						1, 30000, 1,
						"Snooze Time:", "min",
						set_snooze_time
					);
					break;
				case SETTING_SNOOZE_MAX:
					start_number_editor(
						setting_give_up_after_aggregate_snooze_time_of,
						10, 30000, 10,
						"Snooze Expire:", "min",
						set_snooze_expire
					);
					break;
				case SETTING_BLINK_COLON:
					theme_settings.flags ^= THEME_FLAG_BLINK_COLON;
					update_settings_visuals(false);
					break;
				case SETTING_ENABLE_BIG_CLOCK:
					theme_settings.flags ^= THEME_FLAG_DISABLE_BIG_CLOCK;
					update_settings_visuals(false);
					break;
				case SETTING_BACKLIGHT_COLOR:
					start_color_editor(
						theme_settings.color_backlight,
						"Backlight Color:",
						set_backlight_color
					);
					break;
				case SETTING_NOTIFICATION_COLOR:
					start_color_editor(
						theme_settings.color_notification,
						"Notify Color:",
						set_notification_color
					);
					break;
				case SETTING_TEST_RING:
					set_notification(NOTIFY_RING, "Test Ring Notification",NOTIFICATION_ORIGIN_NONE, 10, 0x1dea);
					ui_go_back();
					break;
				case SETTING_DUMP_EEPROM:
					ui_set_message(F("Dumping ..."));
					dump_eeprom();
					update_settings_visuals(false);
					break;
				case SETTING_FORMAT_EEPROM:
					ui_set_message(F("Formatting ..."));
					format_eeprom();
					delay(100);
					update_settings_visuals(false);
					break;
			}
			return;
		case BTN_UP:
			if (settings_index > 0) {
				settings_index -= 1;
			}
			break;
		case BTN_DOWN:
			if (settings_index < SETTING_END_OF_MENU-1) {
				settings_index += 1;
			}
			break;
	}
	// Only fall through when staying in the settings menu
	update_settings_visuals(false);
}

void set_snooze_time(int snooze_time) {
	setting_minutes_snooze_time = snooze_time;
}

void set_snooze_expire(int snooze_expire) {
	setting_give_up_after_aggregate_snooze_time_of = snooze_expire;
}

void set_backlight_color(color_t color) {
	theme_settings.color_backlight = color;
}

void set_notification_color(color_t color) {
	theme_settings.color_notification = color;
}
