typedef void (*ColorEditorHandler)(color_t color);

color_t color_editor_acc;
ColorEditorHandler color_editor_finish_handler = NULL;
ui_mode_t color_editor_previous_mode = UI_INACTIVE;
uint32_t color_editor_cursor = 0;

void start_color_editor(color_t color_to_edit, char* title, void (*finish_handler)(color_t)) {
	color_editor_previous_mode = ui_mode;
	ui_mode = COLOR_EDITOR;
	color_editor_finish_handler = finish_handler;
	color_editor_acc = color_to_edit;
	color_editor_cursor = 0;
	ui_clear();
	ui_set_title(title);
	// Only use a normal cursor because
	// the blinking one is bloody confusing
	// in color editor mode
	lcd.cursor();
	update_color_editor_visuals();
}

// Make surethe color editor format is exactly 16 chars long
const char color_editor_format[] = " r:00 g:00 b:00 ";
const char color_editor_cursor_positions[] = {3,8,13};

void update_color_editor_visuals() {
	char color_chars[17] = {};
	for (char i=0; i<16; i++) {
		color_chars[i] = color_editor_format[i];
	}
	color_chars[16] = 0;
	write_hex_value(color_editor_acc.r, color_chars+3);
	write_hex_value(color_editor_acc.g, color_chars+8);
	write_hex_value(color_editor_acc.b, color_chars+13);
	ui_clear_lcd_line(1);
	lcd.setCursor(0,1);
	lcd.print(color_chars);
	lcd.setCursor(color_editor_cursor_positions[color_editor_cursor],1);
	set_rgb_color(color_editor_acc);
}

void color_editor_handle_button(int button, uint8_t repeat_count) {
	int delta = 1;
	switch (button) {
		case BTN_LEFT:
			if (color_editor_cursor > 0) {
				color_editor_cursor--;
			} else if (repeat_count) {
				//cancel
				color_editor_finish_handler = NULL;
				switch_ui_mode_to(color_editor_previous_mode);
				return;
			}
			break;
		case BTN_RIGHT:
			if (color_editor_cursor < 2) {
				color_editor_cursor++;
			}
			break;
		case BTN_DOWN:
			delta = -1;
			// Falltrough intentional
		case BTN_UP:
			if (repeat_count > 7) {
				// Multiply by 8 in cheap
				delta = delta << 3;
			}
			if (color_editor_cursor < 3) {
				int c = ((uint8_t*) &color_editor_acc)[color_editor_cursor]+delta;
				if ( c > 0xFF) {
					c = 0xff;
				} else if (c < 0) {
					c = 0;
				}
				((uint8_t*) &color_editor_acc)[color_editor_cursor] = c;
			}
			break;
		case BTN_OK:
			if (color_editor_finish_handler != NULL) {
				(*color_editor_finish_handler)(color_editor_acc);
			}
			//set LEDs according to the new ui mode that has been set by the handler.
			update_leds();
			flag_unsaved_data();
			color_editor_finish_handler = NULL;
			switch_ui_mode_to(color_editor_previous_mode);
			return;
	}
	update_color_editor_visuals();
}
