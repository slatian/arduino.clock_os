typedef void (*NumberEditorHandler)(int number);

int number_editor_acc;
int number_editor_max = 0xFF;
int number_editor_min = 0;
int number_editor_step = 1;
String number_editor_unit = "";
ui_mode_t number_editor_previous_mode = UI_INACTIVE;
NumberEditorHandler number_editor_finish_handler = NULL;

void start_number_editor(int number_to_edit, int min, int max, int step, char* title, String unit, void (*finish_handler)(int)) {
	number_editor_previous_mode = ui_mode;
	number_editor_finish_handler = finish_handler;
	ui_mode = NUMBER_EDITOR;
	ui_clear();
	ui_set_title(title);
	number_editor_acc = number_to_edit;
	number_editor_max = max;
	number_editor_min = min;
	number_editor_step = step;
	number_editor_unit = unit;
	update_number_editor_text();
}

void update_number_editor_text() {
	//Optimization: split using append
	ui_set_message(String(number_editor_acc));
	ui_append_message(" ");
	ui_append_message(number_editor_unit);
	ui_update_message();
}

void number_editor_handle_button(int button, uint8_t repeat_count) {
	int delta = number_editor_step;
	switch(button) {
		case BTN_OK:
		case BTN_RIGHT:
			if (number_editor_finish_handler != NULL) {
				(*number_editor_finish_handler)(number_editor_acc);
			}
			flag_unsaved_data();
			// Falltrough intenteional
		case BTN_LEFT:
			// Free up some memory
			number_editor_unit = "";
			// Prevent accidents
			switch_ui_mode_to(number_editor_previous_mode);
			return;
		case BTN_DOWN:
			delta = -delta;
			//Falltrough intentional
		case BTN_UP:
			if (repeat_count > 18) {
				delta = delta*100;
			} else if (repeat_count > 9) {
				delta = delta*10;
			}
			number_editor_acc += delta;
			if (number_editor_acc > number_editor_max) {
				number_editor_acc = number_editor_max;
			} else if (number_editor_acc < number_editor_min) {
				number_editor_acc = number_editor_min;
			}
			update_number_editor_text();
			break;
	}
}
