#ifndef timekeeping_h_INCLUDED
#define timekeeping_h_INCLUDED

#include <Wire.h>
#include <RTClib.h>

// Save some calls to rtc.now()
DateTime now = DateTime();

//Disable real rtc if we don't find it
bool real_rtc_active = false;

// Flag that gets set whenever the clocks get adjusted 
bool rtc_was_adjusted = false;

// Wheter or not we are using summertime (1 hour added)
bool timekeeping_is_on_summertime = false;

// Set new time
void adjust_clocks(DateTime new_now);

// Update the now variable with new time from rtc
void timekeeping_update_now();

// Adjust software RTC with hardware RTC if it is available.
void timekeeping_syncronize_rtc();

// Initalize the clocks
bool timekeepinng_initalize_clocks(TwoWire* wire);

#endif // timekeeping_h_INCLUDED
