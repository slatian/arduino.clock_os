#ifndef Clock_v1_hardware_h_INCLUDED
#define Clock_v1_hardware_h_INCLUDED

//**** LCD Pins ****//

#define LCD_RS_PIN 4
#define LCD_EN_PIN 2
#define LCD_D4_PIN 3
#define LCD_D5_PIN 6
#define LCD_D6_PIN 7
#define LCD_D7_PIN 8

//**** Buttons ****//

#define BTN_OK A1
#define BTN_UP 12
#define BTN_DOWN A2
#define BTN_LEFT A3
#define BTN_RIGHT A0

//**** OTHER OUTPUTS ****//

// For now the LED just blinks once a second
// and twice a second if there is data that has not been saved to EEPROM
#define LED_PIN 13

// RGB Main indicator and LCD backlight LED
#define RGB_R_PIN 5
#define RGB_G_PIN 10
#define RGB_B_PIN 9

// Will annoy you until you wake up and tell it to shut up
#define BEEPER_PIN 11


#endif // Clock_v1_hardware_h_INCLUDED

