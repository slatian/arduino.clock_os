/*
 	Alarm Editor for Clock OS Verion 1

	Provides:
	* update_alarm_editor_visuals()
		Callback to restore the visuals of the alarm editor
	* update_alarm_selector_visuals()
		Callback to restore the visuals of the alarm selector
	* alarm_selector_handle_button(int button)
		Callback when a button was pressed in alarm selector mode
	* alarm_editor_handle_button(int button)
		Callback when a button was pressed in alarm editor mode

	Needs:
	* An LCD Object called `lcd` that drives a 16x2 caracter lcd
	* A defined `N_ALARMS` and an `alarms[N_ALARMS]` array
	* A global `ui_mode` variable and an enum with ALARM_EDITOR and ALARM_SELECTOR as possible entries
	* The `flag_unsaved_data()` and `save_to_eeprom()` functions from the eeprom module
	* The `start_clock_face()` function for returning from the selector
*/

#include "alarm.h"

#define N_ALARM_EDITOR_ENTRYS 14
byte currently_selected_alarm_editor_entry = 0;

const char* label_set_daily = "SET Daily";
const char* label_set_m_to_f = "SET on Mon to Fri";
const char* label_set_monday = "Monday";
const char* label_set_tuesday = "Tuesday";
const char* label_set_wednesday = "Wednesday";
const char* label_set_thursday = "Thursday";
const char* label_set_friday = "Friday";
const char* label_set_saturday = "Saturday";
const char* label_set_sunday = "Sunday";
const char* label_delete_alarm = "DELETE ALARM";
const char* label_unsnooze = "*Zzz* Unsnooze";
const char* label_alarm_enable = "Enable Alarm";
const char* label_prefix_snooze_time = "Snooze Time ";
const char* title_select_alarm = "Select Alarm:";

const char* empty_string = "";


const char* alarm_editor_labels[] = {
	empty_string, //Enable / disable
	empty_string, //Time
	label_set_daily,
	label_set_m_to_f,
	label_set_monday,
	label_set_tuesday,
	label_set_wednesday,
	label_set_thursday,
	label_set_friday,
	label_set_saturday,
	label_set_sunday,
	empty_string,
	empty_string,
	label_delete_alarm
};


void update_alarm_selector_visuals() {
	currently_selected_alarm = min(currently_selected_alarm, N_ALARMS);
	ui_set_title(title_select_alarm);
	ui_set_menu_arrows(currently_selected_alarm, currently_selected_alarm+1 < N_ALARMS);
	alarm_t *alarm = &alarms[currently_selected_alarm];
	char *message_buf = "0>00:00 ";
	message_buf[0] = '0'+(currently_selected_alarm%10);
	alarm_time_to_string(alarm->settings.minutes_after_midnight, message_buf+2);
	ui_set_message(message_buf);
	if (alarm->minutes_snooze_time > 0) {
		ui_append_message("(zzz ");
		ui_append_message(String(alarm->minutes_snooze_time).c_str());
		ui_append_message("m) ");
	}
	ui_append_message(alarm_state_to_summary_string(alarm, false).c_str());
	ui_update_message();
}

void update_alarm_editor_visuals() {
	alarm_t *alarm = &alarms[currently_selected_alarm];
	ui_set_title("Alrm "+String(currently_selected_alarm)+" "+alarm_state_to_summary_string(alarm, true));
	ui_set_menu_arrows(currently_selected_alarm_editor_entry, currently_selected_alarm_editor_entry < 13);
	if (currently_selected_alarm_editor_entry == 0) {
		if (alarm->minutes_snooze_time > 0) {
			ui_set_message(label_unsnooze);
		} else {
			ui_set_switch_message(label_alarm_enable, alarm->settings.flags&0x80);
		}
	} else if (currently_selected_alarm_editor_entry == 1) {
		char *message_buf = "Time: [00:00]";
		alarm_time_to_string(alarm->settings.minutes_after_midnight, message_buf+7);
		ui_set_message(message_buf);
	} else if (currently_selected_alarm_editor_entry == 11) {
		ui_set_message(label_prefix_snooze_time);
		if (alarm->settings.snooze_delay_minutes) {
			ui_append_message("[");
			ui_append_message(String(alarm->settings.snooze_delay_minutes).c_str());
			ui_append_message(" min]");
		} else {
			ui_append_message("[default]");
		}
	} else if (currently_selected_alarm_editor_entry == 12) {
		ui_set_message("Give up snooze after ");
		if (alarm->settings.snooze_max_repeat) {
			ui_append_message("[");
			ui_append_message(String(alarm->settings.snooze_max_repeat).c_str());
			ui_append_message("]");
		} else {
			ui_append_message("[default]");
		}
	} else if (currently_selected_alarm_editor_entry > 3 && currently_selected_alarm_editor_entry < 11) {
		byte relevant_bit = (currently_selected_alarm_editor_entry-3)%7;
		ui_set_switch_message(
			alarm_editor_labels[currently_selected_alarm_editor_entry],
			alarm->settings.flags&(0x01<<relevant_bit)
		);
	} else {
		ui_set_message(alarm_editor_labels[currently_selected_alarm_editor_entry]);
	}
}

void alarm_editor_handle_button(int button) {
	switch (button) {
		case BTN_DOWN:
			if (currently_selected_alarm_editor_entry+1 < N_ALARM_EDITOR_ENTRYS)
				currently_selected_alarm_editor_entry++;
			break;
		case BTN_UP:
			if (currently_selected_alarm_editor_entry) {
				currently_selected_alarm_editor_entry--;
			}
			break;
		case BTN_LEFT:
			ui_go_back();
			return;
		case BTN_OK:
		case BTN_RIGHT:
			alarm_t *alarm = &alarms[currently_selected_alarm];
			switch (currently_selected_alarm_editor_entry) {
				case 0: // enable / disable
					if (alarm->minutes_snooze_time) {
						alarm->minutes_snooze_time = 0;
					} else {
						// Toggle enable bit
						alarm->settings.flags ^= 0x80;
					}
					break;
				case 1: // set time
					start_time_editor(TIME_EDITOR, SET_ALARM);
					return; //return because we switched ui mode
				case 2: // Daily
					alarm->settings.flags = 0xff;
					break;
				case 3: // Mon to Fri
					alarm->settings.flags = 0xbe;
					break;
				case 11: //Custom snooze time
					start_number_editor(
						alarm->settings.snooze_delay_minutes,
						0,255,1,
						"Snooze Delay:", "min",
						set_alarm_snooze_time
					);
					return; //return because we switched ui mode
				case 12: //Custom snooze max repeat
					start_number_editor(
						alarm->settings.snooze_max_repeat,
						0,255,1,
						"Snooze Max:", "times",
						set_alarm_snooze_max_repeat
					);
					return; //return because we switched ui mode
				case 13: //Delete
					alarm->settings.flags = 0x00;
					alarm->settings.minutes_after_midnight = 0;
					break;
				default:
					byte relevant_bit = (currently_selected_alarm_editor_entry-3)%7;
					// toggle the relevant day of week bit using xor
					alarm->settings.flags ^= (0x01<<relevant_bit);
					break;
			}
			//usually pressing one of these buttons alters something we want to save
			flag_unsaved_data();
			break;
	}
	update_alarm_editor_visuals();
}

void alarm_selector_handle_button(int button) {
	switch (button) {
		case BTN_DOWN:
			if (currently_selected_alarm+1 < N_ALARMS)
				currently_selected_alarm++;
			break;
		case BTN_UP:
			if (currently_selected_alarm) {
				currently_selected_alarm--;
				break;
			}
			// Fallthrough intentional
		case BTN_LEFT:
			save_to_eeprom(false);
			start_clock_face();
			return;
		case BTN_OK:
		case BTN_RIGHT:
			ui_mode = ALARM_EDITOR;
			currently_selected_alarm_editor_entry = 0;
			update_alarm_editor_visuals();
			return;
	}
	update_alarm_selector_visuals();
}

void set_alarm_snooze_time(int minutes) {
	alarm_t *alarm = &alarms[currently_selected_alarm];
	alarm->settings.snooze_delay_minutes = (uint8_t) minutes;
}

void set_alarm_snooze_max_repeat(int n) {
	alarm_t *alarm = &alarms[currently_selected_alarm];
	alarm->settings.snooze_max_repeat = (uint8_t) n;
}

