/*
	This file contains the serial interface for Clock OS Version 1.
	While machine readable it is made for debugging by hand and will be replaced with something better once I come up with something better
*/

/*
	Provides:
	* startup_serial_interface()
		Initalizes the module, call in startup()

	* task_serial_interface()
		Reads and parses the serial input, run periodically

	Needs:
	* A global `notification` object
	* Access to the `Serial` object
	* An `adjust_clocks(DateTime new_now)` function
	* A global `DateTime now` object
	* A defined `N_ALARMS` and an array `alarms[N_ALARMS]`
	* The `set_notification(mode, command, NOTIFICATION_ORIGIN_EXTERNAL, expires_in, remote_id)` function
*/

#include <RTClib.h>

#include "notification.h"
#include "serial_protocol.h"


#define INBOX_BUFFER_LIMIT 128
byte inbox_buffer[INBOX_BUFFER_LIMIT] = {};
byte inbox_cursor = 0;

enum serial_receiver_state_t {
	WAIT_FOR_ZERO,
	WAIT_FOR_LENGTH,
	RECEIVE_DATA,
	IGNORE_DATA,
};

serial_receiver_state_t serial_receiver_state = WAIT_FOR_ZERO;
byte incoming_packet_data_length = 0;

inline void startup_serial_interface() {
	Serial.begin(9600);
	Serial.setTimeout(100);
	Serial.println(F("//STARTUP Clock OS v1"));
}

inline void task_serial_interface() {
	handle_serial_input();
}

/*****************************************************************************/

void handle_serial_input() {
	byte last_read = 0;
	while (Serial.available()) {
		last_read = Serial.read();
		switch (serial_receiver_state) {
			case WAIT_FOR_ZERO:
				if (last_read == 0) {
					serial_receiver_state = WAIT_FOR_LENGTH;
				}
				break;
			case WAIT_FOR_LENGTH:
				incoming_packet_data_length = last_read;
				if (last_read == 0) {
					// Send the 00 reply and stay in the WAIT_FOR_LENGTH state.
					Serial.write(0);
					Serial.write(0);
				// The '-1' is to keep a nullbyte at the end of the data
				} else if (incoming_packet_data_length < INBOX_BUFFER_LIMIT-1) {
					serial_receiver_state = RECEIVE_DATA;
					inbox_cursor = 0;
				} else {
					inbox_cursor = 0;
					serial_receiver_state = IGNORE_DATA;
				}
				break;
			case RECEIVE_DATA:
				inbox_buffer[inbox_cursor] = last_read;
				inbox_cursor++;
				if (inbox_cursor >= incoming_packet_data_length) {
					on_incoming_packet_for_me(inbox_buffer, incoming_packet_data_length);
					// Make sure the inbox is zeroed out
					for (int i = 0; i<INBOX_BUFFER_LIMIT; i++) {
						inbox_buffer[i] = 0;
					}
					serial_receiver_state = WAIT_FOR_ZERO;
				}
				break;
			case IGNORE_DATA:
				inbox_cursor++;
				if (inbox_cursor >= incoming_packet_data_length) {
					serial_receiver_state = WAIT_FOR_ZERO;
				}
				break;
		}
	}
}

void on_incoming_packet_for_me(byte* packet_buffer, byte packet_length) {
	byte* data_buffer = packet_buffer+1;
	packet_length--;
	switch (packet_buffer[0]) {
		case SERIAL_PROTOCOL_RTC_EXCHANGE:
			handle_rtc_exchange_incoming(
				(rtc_exchange_protocol_data_t*)data_buffer,
				packet_length
			);
			break;
		case SERIAL_PROTOCOL_NOTIFICATION:
			handle_notification_exchange_incoming(
				(notification_exchange_protocol_data_t*)data_buffer,
				packet_length
			);
			break;
	}
}

void send_serial_packet(byte protocol, byte *data, byte length) {
	Serial.write(0);
	Serial.write(length+1);
	Serial.write(protocol);
	for (byte i = 0; i<length; i++) {
		Serial.write(data[i]);
	}
}

/*** RTC Exchange ***/

void handle_rtc_exchange_incoming(struct rtc_exchange_protocol_data_t *data, byte packet_length) {
	switch (data->control) {
		case DMAN_REQUEST:
			send_rtc_exchange_information(DMAN_ANNOUNCE);
			break;
		case DMAN_ANNOUNCE:
			if (real_rtc_active || rtc_was_adjusted) { break; }
			// the other timesource is probably better than ours, proceed.
		case DMAN_SET:
			DateTime new_time = DateTime((uint32_t)data->timestamp);
			adjust_clocks(new_time);
			send_rtc_exchange_information(DMAN_ANNOUNCE);
			break;
	}
}

void send_rtc_exchange_information(byte verb) {
	rtc_exchange_protocol_data_t data;
	data.control = verb;
	if (rtc_was_adjusted)
		data.flags += RTC_EXCHANGE_WAS_ADJUSTED;
	if (real_rtc_active)
		data.flags += RTC_EXCHANGE_IS_BATTERY_BACKED;
	data.timestamp = (int64_t) now.unixtime();
	// Send without timezone information
	send_serial_packet(16, (byte*)&data, sizeof(data)-4);
}


/*** Notification Exchange ***/

void handle_notification_exchange_incoming(struct notification_exchange_protocol_data_t *data, byte packet_length) {
	switch (data->control) {
		case DMAN_REQUEST:
			send_notification(0);
			break;
		case DMAN_PUT:
			//TODO
			break;
		case DMAN_SET:
			// Trigger an action on the notification
			if (notification.mode == NO_NOTIFICATION) { return; }
			switch (packet_length) {
				case 8:
					if (data->serial != notification.serial)
						return;
					break;
				case 12:
					if (data->cookie != notification.cookie)
						return;
					break;
				case 20:
					if (data->timestamp_sent != (int64_t) notification.time_sent.unixtime())
						return;
					break;
					
				default:
					return;
			}
			switch (data->action) {
				case 'd':
				case 'a':
					dismiss_notification(data->action);
			}
			break;
	}
}

void send_notification(char action) {
	if (notification.mode == NO_NOTIFICATION) { return; }
	notification_exchange_protocol_data_t data;
	data.control = DMAN_ANNOUNCE;
	data.action = action;
	data.severity = (3-(notification.mode))&3;
	data.serial = notification.serial;
	data.cookie = notification.cookie;
	data.timestamp_sent = (int64_t) notification.time_sent.unixtime();
	data.timestamp_expire = (int64_t) notification.expires_at.unixtime();
	data.message_size = 254-sizeof(data);
	int message_length = notification.message.length();
	if (message_length < data.message_size) {
		data.message_size = message_length;
	}
	
	Serial.write(0);
	Serial.write(sizeof(data)+data.message_size+1);
	Serial.write(17);
	byte* serial_data = (byte*)&data;
	for (byte i = 0; i<sizeof(data); i++) {
		Serial.write(serial_data[i]);
	}
	Serial.print(notification.message.substring(0,data.message_size));
}
