#ifndef color_h_INCLUDED
#define color_h_INCLUDED

struct color_t {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

color_t u32_to_color(uint32_t);

#endif // color_h_INCLUDED
