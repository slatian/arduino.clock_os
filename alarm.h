#ifndef alarm_h_INCLUDED
#define alarm_h_INCLUDED

#include <RTClib.h>

/*
 * Alarm flags:
 * 0x80 enabled
 * 0x40 Saturday
 * 0x20 Friday
 * 0x10 Thursday
 * 0x08 Wednesday
 * 0x04 Tuesday
 * 0x02 Monday
 * 0x01 Sunday
 *
 * Enabled without a weekday flag means to only ring once
 * the next time the set time matches. (And disable the alarm afterwards)
 */ 

// alarm data to be persisted
struct alarm_settings_t {
	// version used by storage mechanism to decide when a format upgrade is neccessary.
	// Leave alone, this should always represent the version in storage.
	uint8_t version;
	uint8_t flags = 0; //0x80 ring once, 0x00 disabled, 0xFF ring daily, 0x80 || 0xXX ring on specified weekdays
	uint16_t minutes_after_midnight = 0;
	// custom snooze settings
	uint8_t snooze_max_repeat = 0;
	uint8_t snooze_delay_minutes = 0;
};

// alarm data on memory
struct alarm_t {
	alarm_settings_t settings;
	// aggregated snooze time
	short minutes_snooze_time = 0;
	// flag used by cronjob to know if the alarm already went off
	bool was_triggerd = false;
	// Storage informantion
	uint8_t drive;
	uint8_t descriptor_id;
	bool is_in_storage = false;
};


bool alarm_is_over_snooze_time(const alarm_t *alarm);

bool alarm_matches_timestamp(const alarm_t *alarm, DateTime match);

// Writes the alarm time in the format of '00:00' to out
// it will always require out to have space for the 5 bytes that get overwritten.
// off_from_midnight is how many minutes have passed since midnight
void alarm_time_to_string(uint16_t off_from_midnight, char* out);


String alarm_state_to_summary_string(alarm_t *alarm, bool ignore_off);

#endif // alarm_h_INCLUDED

