void start_big_clock() {
	ui_mode = BIG_CLOCK;
	ui_clear();
	ui_initalize_big_digits();
	update_big_clock();
}

void update_big_clock() {
	if (clock_should_display_big_digits()) {
		ui_draw_big_colon_at(7, !(theme_settings.flags & THEME_FLAG_BLINK_COLON && now.second()%2));
		ui_draw_big_digit_at(0, now.hour()/10);
		ui_draw_big_digit_at(4, now.hour()%10);
		ui_draw_big_digit_at(9, now.minute()/10);
		ui_draw_big_digit_at(13, now.minute()%10);
	} else {
		ui_go_back();
	}
}
