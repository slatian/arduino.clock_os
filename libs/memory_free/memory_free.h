#ifndef memory_free_h_INCLUDED
#define memory_free_h_INCLUDED

#include <Arduino.h>

int freeMemory();

#endif // memory_free_h_INCLUDED
