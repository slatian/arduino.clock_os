#include "alarm.h"

// Returns true if the alarm shouldn't repeat after dismissing with its current snooze time
bool alarm_is_over_snooze_time(const alarm_t *alarm) {
	if (alarm->settings.snooze_max_repeat) {
		int repetition_delay = (int) setting_minutes_snooze_time;
		if (alarm->settings.snooze_delay_minutes) {
			repetition_delay = (int) alarm->settings.snooze_delay_minutes;
		}
		return alarm->minutes_snooze_time >= (repetition_delay*alarm->settings.snooze_max_repeat);
	} else {
		return alarm->minutes_snooze_time >= setting_give_up_after_aggregate_snooze_time_of;
	}
}

bool alarm_matches_timestamp(const alarm_t *alarm, DateTime match) {
	if (alarm->settings.flags & 0x80) {
		if (
			alarm->settings.flags == 0x80 ||
			alarm->settings.flags & (0x01<<match.dayOfTheWeek())
		) {
			return (alarm->settings.minutes_after_midnight+alarm->minutes_snooze_time)%(1440) == match.hour()*60+match.minute();
		}
	}
	return false;
}

/* !! UNTESTED, Test before use !!
// Calculate in how many minutes the alarm will ring,
// will return a value smaller than 0 if the alarm will never ring.
long alarm_rings_in_n_minutes(const alarm_t *alarm, DateTime from) {
	if (alarm->settings.flags & 0x80) {
		long base_offset = (alarm->settings.minutes_after_midnight+alarm->minutes_snooze_time) -  match.hour()*60+match.minute();
		if (alarm->settings.flags == 0x80) {
			if (base_offset < 0) {
				//add a day
				return base_offset+1440;
			}
		}
		for (uint8_t i = 0; i<8; i++) {
			if (base_offset => 0) {
				uint8_t day_of_week = (from.dayOfTheWeek()+i)%7;
				if (alarm->settings.flags & (0x01<<day_of_week) {
					return base_offset;
				}
			}
			base_offset += 1440;
		}
	}
	return -1;
}
*/

// Writes the alarm time in the format of '00:00' to out
// it will always require out to have space for the 5 bytes that get overwritten.
// off_from_midnight is how many minutes have passed since midnight
void alarm_time_to_string(uint16_t off_from_midnight, char* out) {
	byte minutes = off_from_midnight%60;
	short hours = off_from_midnight/60;
	out[0] = '0'+hours/10;
	out[1] = '0'+hours%10;
	out[2] = ':';
	out[3] = '0'+minutes/10;
	out[4] = '0'+minutes%10;
}

String alarm_state_to_summary_string(alarm_t *alarm, bool ignore_off) {
	byte flags=alarm->settings.flags;
	if (flags==0 && (!ignore_off || alarm->settings.minutes_after_midnight == 0)) { return "- FREE -"; }
	if (!(flags & 0x80) && !ignore_off) { return "off"; }
	if (flags==0xff || flags==0x7f) { return "Daily"; }
	if (flags==0x80) { return "Once"; }
	char *state = "        ";
	// Statically allocated buffer needs cleaning up
	for (byte i = 0; i<7; i++) { state[i]=' '; }
	if (flags & 0x40) { state[5] = 'S'; } //Saturday, ISO Weekday numbering …
	if (flags & 0x20) { state[4] = 'F'; }
	if (flags & 0x10) { state[3] = 'T'; }
	if (flags & 0x08) { state[2] = 'W'; }
	if (flags & 0x04) { state[1] = 'T'; }
	if (flags & 0x02) { state[0] = 'M'; }
	if (flags & 0x01) { state[6] = 'S'; } //Sunday in bit zero, will make things easier
	return state;
}

