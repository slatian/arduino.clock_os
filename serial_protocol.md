# ClockOS Serial protocol v3

ClockOS speaks a packet based serial protocol.

Each packet starts with a null-byte maker and a one byte length of the data that comes after. The first byte of the data is the packet type.

```
<0><len>[<type><data ...>]
```

## Syncronization

To make it easier to syncronize the protocol state machines, if a zero length packet (a `00` packet) arrives the other side is supposed to reply with a `00` packet.

Simple syncronization would then send some zeros and once replies come back one just has to figure out wheter we are one byte off which can be done by sending one zero, waiting for a reply and of no reply comes back we know that we are in sync.

## Protocols

Protocols IDs 0 to 15 are reserved for core services, so any non-core services start at 15.

| ID | Protocol     |
| 16 | RTC Exchange |
| 17 | Notification |

## Common constants

Some Protocols require some constatnts that are also useful for other protocols.

### Data manipulation (DMAN)

This enum is represented using one byte.

| # | Meaning                                  |
|---|------------------------------------------|
| 0 | REQUEST the value from the peer          |
| 1 | ANNOUNCE own value (response to REQUEST) |
| 2 | SET value to the peer                    |
| 3 | PUT (similar to HTTP PUT)                |

## RTC Exchange Protocol (#16)

The RTC Exchange Protocol should fullfill the role of allowing to exchange time information and remotely adjust clocks. It must include enoughinformation about the clock to allow automatic clock setting.

for a get request the

Information must include:
* Is the clock battery backed (1)
* Was the clock adjusted (2)
* The timezone information is accurate (4)

A battery backed clock is more trustworthy than a non battery backed one and between non-battery backed ones the on that remembers being adjusted wins out.

The packet contains first a command byte. Then a flag field and then an 8 byte signed integer unix timestamp in seconds local time, followed by an optional 4 byte signed integer that contains the timezone offset from utc in seconds, this can be used by more advanced timekeeping to implement timezone aware things while simpler software can just ignore it.

| offset | size | content                                  |
|--------|------|------------------------------------------|
| 0      | 1    | command, either REQUEST, ANNOUNCE or SET |
| 1      | 1    | flags                                    |
| 2      | 8    | timestamp (local unixtime, seconds)      |
| 10     | 4    | timezone offset from utc in seconds      |

for a request all other fields are optional and to be ignored if present.

## Notification Protocol (#17)

This protocol allows sending and receiving simple notifications.

A notification consists of the following parts:
* A severity
	* low (0)
	* normal (1)
	* urgent (2)
	* ringing (3)
* A timeout in seconds
* A few characters of text message
* A notification id

A notification can end for one of the following reasons:
* Dismiss (i.e. snooze) `d`
* Acknowlege (as in I've read, take action) `a`
* Expire `e`

If the system can only deal with a limited amount of notifications (even just one), notifications may be dismissed automatically.

The following notification operatios are possible:
* GET a given notification
* Dismiss or Acknowlege a notification (using SET)
* PUT up a new notification
* ANNOUNCE the state of a notification

The data structure sent for PUT ing and ANNOUNCE ing the notification protocol looks like the following:

| offset | size | description                                      |
|--------|------|--------------------------------------------------|
| 0      | 1    | DMAN command                                     |
| 2      | 1    | notification action or null                      |
| 3      | 1    | notification severity (bits 2-7 reserved, ignore)|
| 4      | 4    | notification serial number                       |
| 8      | 4    | notification cookie                              |
| 12     | 8    | timestamp the notification was sent              |
| 20     | 8    | timestamp the notification expires at (unix utc) |
| 28     | 1    | notification text length                         |
| 29     | n    | notification text                                |

The notification flags are:

| # | description |
|---|-------------|
| 1 | empty, set for replies when the notification is empty |

When a get request is issued the device should announce all notifications available.
