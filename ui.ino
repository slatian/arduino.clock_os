#include "ui.h"

/*
 * custom chars:
	* 0: 
	* 1: big colon a
	* 2: big colon b
	* 3: arrow up and/or down
    * 4:
	* 5: switch part 1 | top half
	* 6: switch part 2 | bottom half
    * 7: full
*/

// Bitmaps flipped by 90 degrees.
// Place space after 4th bit to
// see the font.
const byte big_pixel_digits[15] = {
	0b00101111, // 1 0
	0b11111001,
	0b00001111,

	0b10011101, // 3 2
	0b11011101,
	0b11111010,

	0b10110111, // 5 4
	0b11011110,
	0b01010100,

	0b00011111, // 7 6
	0b00011101,
	0b11111101,

	0b10111111,  // 9 8
	0b10111101,
	0b11111111

};

const byte icon_parts[12] = {
	0b00000, // 0
	0b11111,
	0b10001, // 2
	0b10101,
	0b11011, // 4
	0b10011,
	0b10000, // 6
	0b00001,
	0b01110, // 8
	0b00100,
	0b00011, // A
	0b11000
};

#define ICON_ARROW_UP      0x98100000
#define ICON_ARROW_DOWN    0x00000189
#define ICON_ARROW_UP_DOWN 0x98100189
#define ICON_SWITCH_ON_A   0x01666100
#define ICON_SWITCH_ON_B   0x14544410
#define ICON_SWITCH_OFF_A  0x12333210
#define ICON_SWITCH_OFF_B  0x01777100
#define ICON_TOP_HALF      0x11110000
#define ICON_BOTTOM_HALF   0x00001111
#define ICON_FULL          0x11111111
#define ICON_HALF_COLON_A  0x00AAAA00
#define ICON_HALF_COLON_B  0x00BBBB00

void ui_initalize() {
	// Currently a noop
}

void upload_icon(byte c, uint32_t parts) {
	byte buffer[8] = {};
	for (byte i = 0; i<8; i++) {
		buffer[7-i] = icon_parts[parts&0xf];
		parts = parts >> 4;
	}
	lcd.createChar(c, buffer);
}

void ui_clear() {
	lcd.clear();
	lcd.noCursor();
	lcd.noBlink();
	ui_set_message("");
}

void ui_clear_lcd_line(byte line) {
	lcd.setCursor(0,line);
	for(byte i = 0; i<16; i++) { lcd.write(' '); }
}

void ui_set_message(String message) {
	if (ui_current_message == message) { return; }
	ui_current_message = message;
	ui_current_message_start_time = millis();
	ui_clear_lcd_line(1);
	ui_update_message();
}

void ui_set_message_with_icon(String message, ui_icon_t icon) {
	switch (icon) {
		case ICON_SWITCH_OFF:
			upload_icon(5, ICON_SWITCH_OFF_A);
			upload_icon(6, ICON_SWITCH_OFF_B);
			ui_set_message("\x05\x06 ");
			break;
		case ICON_SWITCH_ON:
			upload_icon(5, ICON_SWITCH_ON_A);
			upload_icon(6, ICON_SWITCH_ON_B);
			ui_set_message("\x05\x06 ");
			break;
		default:
			return;
	}
	ui_append_message(message);
	ui_update_message();
}

void ui_set_switch_message(String message, bool value) {
	ui_set_message_with_icon(message, value?ICON_SWITCH_ON:ICON_SWITCH_OFF);
}

void ui_set_message(char* message) {
	ui_set_message(String(message));
}

// Appends at the end of the current message
// the caller is responsible for calling
// ui_update_message after they are done appending
void ui_append_message(char* message) {
	ui_current_message += message;
}

void ui_append_message(String message) {
	ui_current_message += message;
}

void ui_update_message() {
	if (ui_current_message == "") { return; }
	unsigned long display_time = millis() - ui_current_message_start_time;
	//Use hardcoded LCD specifica here
	lcd.setCursor(0,1);
	unsigned int message_length = ui_current_message.length();
	if (message_length > 16) {
		// we can make this assumption because of our if here
		int total_time = (message_length-16)*500+2500;
		int offset = constrain((int) ((((int) (display_time%total_time))-1500)/500), 0, message_length-16);
		lcd.print(ui_current_message.substring(offset,offset+16));
	} else {
		lcd.print(ui_current_message);
	}
}

void ui_set_menu_arrows(bool up, bool down) {
	byte c = 3;
	if (up && down) {
		upload_icon(3, ICON_ARROW_UP_DOWN);
	} else if (up) {
		upload_icon(3, ICON_ARROW_UP);
	} else if (down) {
		upload_icon(3, ICON_ARROW_DOWN);
	} else {
		c = ' ';
	}
	lcd.setCursor(15,0);
	lcd.write(c);
}

void ui_set_title(String title) {
	if (title != "") {
		ui_clear_lcd_line(0);
		lcd.setCursor(0,0);
		//assume a lcd width of 16 here
		lcd.print(title.substring(0,min(16,title.length())));
	}
}

/* Currently not enough users
void ui_set_title(char* title) {
	if (title != "") {
		clear_lcd_line(0);
		lcd.setCursor(0,0);
		//assume a lcd width of 16 here
		for (uint8_t i = 0; i <	16 && title[i]; i++) {
			lcd.print(title[i]);
		}
	}
}
*/

void ui_initalize_big_digits() {
	upload_icon(1, ICON_HALF_COLON_A);
	upload_icon(2, ICON_HALF_COLON_B);
	
	upload_icon(5, ICON_TOP_HALF);
	upload_icon(6, ICON_BOTTOM_HALF);
	upload_icon(7, ICON_FULL);
}

void ui_draw_big_digit_part(byte x, byte y, byte bitmap) {
	char c = ' ';
	if (bitmap) {
		c = 4+bitmap;
	}
	lcd.setCursor(x,y);
	lcd.write(c);
}

void ui_draw_big_digit_at(byte x_offset, byte digit) {
	byte index = (digit >> 1)*3;
	for ( byte i = 0; i<3; i++) {
		byte a = big_pixel_digits[index+i];
		if (digit & 0x01) {
			a = a >> 4;
		}
		ui_draw_big_digit_part(x_offset+i, 0, a & 0x03);
		ui_draw_big_digit_part(x_offset+i, 1, (a >> 2) & 0x03);
	}
}

void ui_draw_big_colon_at(byte x_offset, bool on) {
	char* sequence = "  ";
	if (on) sequence = { "\x01\x02" };
	lcd.setCursor(x_offset, 0);
	lcd.print(sequence);
	lcd.setCursor(x_offset, 1);
	lcd.print(sequence);
}
